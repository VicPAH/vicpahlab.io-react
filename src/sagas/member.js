import _ from 'lodash';
import { toast } from 'react-semantic-toasts';
import { all, call, put, select, take, takeEvery } from 'redux-saga/effects';

import { getMember, httpErrorState } from '../store/actions';


function* handleGetMember({ id }) {
  yield put({ type: 'GET_MEMBER_STATE', state: 'pending', id });
  try {
    let client;
    while (!client) {
      client = yield select(state => state.refreshClient);
      if (!client) yield take('SET_REFRESH');
    }
    const resp = yield call(() => client.get(`/member/${id}`));
    yield put({ type: 'SET_MEMBER', id, data: resp.data });
    yield put({ type: 'GET_MEMBER_STATE', state: 'success', id });
  } catch(err) {
    yield put(httpErrorState('GET_MEMBER', err, { id }));
  }
}
function* handlePatchMember({ type, id, ...data }) {
  yield put({ type: 'PATCH_MEMBER_STATE', state: 'pending', id });
  try {
    let client;
    while (!client) {
      client = yield select(state => state.refreshClient);
      if (!client) yield take('SET_REFRESH');
    }
    const resp = yield call(() => client.patch(`/member/${id}`, data));
    yield put({ type: 'SET_MEMBER', id, data: resp.data });
    yield put({ type: 'PATCH_MEMBER_STATE', state: 'success', id });
  } catch(err) {
    yield put(httpErrorState('PATCH_MEMBER', err, { id }));
  }
}


function* handleSetRefresh({ payload: { sub } }) {
  yield put(getMember(sub));
}


function* handleRegister(action) {
  const data = _.omit(action, ['type']);
  let client = yield select(state => state.guestClient);
  if (!client) {
    console.error('no guest client');
    return;
  }
  try {
    yield put({ type: 'REGISTER_STATE', state: 'pending' });
    yield call(() => client.post('/register', data));
    yield put({ type: 'REGISTER_STATE', state: 'success' });
    toast({ title: 'Registration complete', type: 'success' });
    yield put({
      type: 'GET_LOGIN_TOKEN',
      username: data.email,
      password: data.password,
    });
  } catch(err) {
    console.error(err);
    yield put(httpErrorState('REGISTER', err));
  }
}


function* handleSubmitApplication({ id }) {
  yield put({ type: 'SUBMIT_APPLICATION_STATE', state: 'pending', id });
  try {
    let client;
    while (!client) {
      client = yield select(state => state.refreshClient);
      if (!client) yield take('SET_REFRESH');
    }
    const resp = yield call(() => client.post(`/submit_application/${id}`));
    yield put({ type: 'SET_MEMBER', id, data: resp.data });
    yield put({ type: 'SUBMIT_APPLICATION_STATE', state: 'success', id });
  } catch(err) {
    yield put(httpErrorState('SUBMIT_APPLICATION', err, { id }));
  }
}
function* handlePayMembership({ id, ...data }) {
  yield put({ type: 'PAY_MEMBERSHIP_STATE', state: 'pending', id });
  try {
    let client;
    while (!client) {
      client = yield select(state => state.refreshClient);
      if (!client) yield take('SET_REFRESH');
    }
    const resp = yield call(() => client.post(
      `/pay/${id}`,
      _.omit(data, ['type']),
    ));
    yield put({ type: 'SET_MEMBER', id, data: resp.data });
    yield put({ type: 'PAY_MEMBERSHIP_STATE', state: 'success', id });
  } catch(err) {
    yield put(httpErrorState('PAY_MEMBERSHIP', err, { id }));
  }
}


export function* watchMember() {
  const sagas = [
    takeEvery('GET_MEMBER', handleGetMember),
    takeEvery('PATCH_MEMBER', handlePatchMember),
    takeEvery('SET_REFRESH', handleSetRefresh),
    takeEvery('REGISTER', handleRegister),
    takeEvery('SUBMIT_APPLICATION', handleSubmitApplication),
    takeEvery('PAY_MEMBERSHIP', handlePayMembership),
  ];
  yield all(sagas);
}
