import _ from 'lodash';
import { createSelector } from 'reselect';

const cc = (c, n) => c ? [c, n] : null;
export const pollsClientSel = createSelector(
  state => cc(state.refreshPollsClient, 'refresh'),
  state => cc(state.guestPollsClient, 'guest'),
  state => cc(state.unauthPollsClient, 'unauth'),
  (...clients) => {
    const [client, clientType] = clients.find(client => !!client);
    return { client, clientType };
  },
);


export const getMember = createSelector(
  (state, props) => _.get(state, ['members', props.id]),
  m => m,
);
export const getMemberState = createSelector(
  (state, props) => _.get(state, ['getMemberStates', props.id]),
  m => m,
);
export const patchMemberState = createSelector(
  (state, props) => _.get(state, ['patchMemberStates', props.id]),
  m => m,
);


export const getLoggedIn = createSelector(
  state => (state.loginPayload || {}).sub,
  state => state.members || {},
  (id, members) => members[id],
);


export const getScopes = createSelector(
  state => _.get(state, ['refreshPayload', 'scope'], ''),
  scope => scope.split(' ').filter(v => v),
);
const createScopeSelector = scope => createSelector(
  getScopes,
  scopes => scopes.indexOf(scope) >= 0,
);
export const isSecretaryScope = createScopeSelector('secretary');
export const isCommitteeScope = createScopeSelector('committee');
// TODO this isn't right, but okay for now
export const isAdminScope = createScopeSelector('secretary');
