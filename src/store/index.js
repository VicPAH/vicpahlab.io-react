import { createBrowserHistory } from 'history';
import { applyMiddleware, compose, createStore } from 'redux';
import { createLogger } from 'redux-logger';

import { sagaMiddleware } from '../sagas';
import { createPollsClient } from '../sagas/auth';

import { rootReducer } from './reducer';


export const HISTORY = createBrowserHistory();


export function configureStore() {
  const middlewares = [sagaMiddleware, createLogger()];
  const middlewareEnhancer = applyMiddleware(...middlewares);

  const enhancers = [middlewareEnhancer];
  const composedEnhancers = compose(...enhancers);

  const store = createStore(
    rootReducer,
    {
      unauthPollsClient: createPollsClient(null, 'unauth'),
    },
    composedEnhancers,
  );

  return store
}


export const STORE = configureStore();
