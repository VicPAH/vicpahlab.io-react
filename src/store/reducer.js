import _ from 'lodash';
import moment from 'moment-timezone';


const setToken = (state, { tokenType, token, client, pollsClient, payload }) => {
  window[`${tokenType}Client`] = client;
  return {
    ...state,
    [`${tokenType}Token`]: token,
    [`${tokenType}Client`]: client,
    [`${tokenType}PollsClient`]: pollsClient,
    [`${tokenType}Payload`]: payload,
  };
};
const deserializeMember = data => ({
  ...data,
  joinDateRaw: data.joinDate,
  joinDate: moment(data.joinDate),
  fullName: [
    ...(data.givenName ? [data.givenName] : []),
    ...(data.familyName ? [data.familyName] : []),
  ].join(' ') || null,
});

const reduceKeyedHttpState = (state, action, stateKey, idPath='id') => ({
  ...state,
  [stateKey]: {
    ...state[stateKey] || {},
    [_.get(action, idPath)]: { state: action.state, errors: action.errors || [] },
  },
});


export const rootReducer = (state, action) => {
  switch(action.type) { // eslint-disable-line default-case
  case 'SET_GUEST':
    return setToken(state, { tokenType: 'guest', ...action });
  case 'SET_LOGIN':
    return setToken(state, { tokenType: 'login', ...action });
  case 'SET_REFRESH':
    return setToken(state, { tokenType: 'refresh', ...action });
  case 'CLEAR_GUEST_CLIENTS':
    return { ...state, guestClient: null, guestPollsClient: null };
  case 'CLEAR_LOGIN_CLIENTS':
    return { ...state, loginClient: null, loginPollsClient: null };
  case 'CLEAR_REFRESH_CLIENTS':
    return { ...state, refreshClient: null, refreshPollsClient: null };
  case 'REGISTER_STATE':
    return { ...state, registerState: action.state, registerErrors: action.errors || [] };
  case 'SET_REGISTER_ERRORS':
    return { ...state, registerErrors: action.errors || [] };
  case 'LOGIN_STATE':
    return { ...state, loginState: action.state, loginErrors: action.errors || [] };
  case 'SET_LOGIN_ERRORS':
    return { ...state, loginErrors: action.errors || [] };
  case 'OAUTH_STATE':
    return { ...state, oauthState: action.state, oauthErrors: action.errors || [] };
  case 'SET_OAUTH_ERRORS':
    return { ...state, oauthErrors: action.errors || [] };
  case 'SET_MEMBER':
    return {
      ...state,
      members: {
        ...state.members || {},
        [action.data.id]: deserializeMember(action.data),
      },
    };
  case 'GET_MEMBER_STATE':
    return reduceKeyedHttpState(state, action, 'getMemberStates');
  case 'PATCH_MEMBER_STATE':
    return reduceKeyedHttpState(state, action, 'patchMemberStates');
  case 'SUBMIT_APPLICATION_STATE':
    return { ...state, submitApplicationState: action.state, submitApplicationErrors: action.errors || [] };
  case 'PAY_MEMBERSHIP_STATE':
    return { ...state, payMembershipState: action.state, payMembershipErrors: action.errors || [] };
  case 'LOGOUT':
    delete localStorage.vicpahLoginToken;
    return {
      ...setToken(setToken(state, { tokenType: 'refresh' }), { tokenType: 'login' }),
      members: {},
    };
  }
  return state;
}
