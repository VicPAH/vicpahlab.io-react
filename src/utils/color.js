export function strToStableChoice(string, choices) {
  return choices[
    string
      .toUpperCase()
      .replace(/[^A-Z]/, '')
      .split('')
      .map(char => char.charCodeAt(0))
      .reduce((sum, i) => sum + i, 0)
      % choices.length
  ]
}
const SEMANTIC_COLORS = [
  'red',
  'orange',
  'yellow',
  'olive',
  'green',
  'teal',
  'blue',
  'violet',
  'purple',
  'pink',
  // 'brown',
  // 'grey',
  // 'black',
];
export function strToColor(string) {
  return strToStableChoice(string, SEMANTIC_COLORS)
}
