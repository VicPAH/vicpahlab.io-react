import { memberService } from 'vic-model-rules-member-fsm';

export const stateCan = (state, name) => state.nextEvents.indexOf(name) >= 0
export const getLogItemDate = logItem => logItem.effectiveDate || logItem.date;
export const stateLogCmp = (left, right) => {
  const lDate = getLogItemDate(left);
  const rDate = getLogItemDate(right);
  if (lDate < rDate) return -1;
  if (lDate > rDate) return 1;
  return 0;
};
export const stateLogToMemberSvc = (stateLog = [], idForLog = null) => {
  const s = memberService().start();
  const stateLogSort = (
    [...stateLog]
      .sort(stateLogCmp)
  );
  for (const logItem of stateLogSort) {
    if (!stateCan(s.state, logItem.action)) {
      console.warn(
        'State change error for',
        idForLog, ':',
        s.state.value,
        'cannot',
        logItem,
      );
    }
    s.send(logItem.action);
  }
  return s;
};
