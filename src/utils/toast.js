import _ from 'lodash';
import React from 'react';
import { toast } from 'react-semantic-toasts';

const renderManyMessages = messages => <ul>{ messages.map(m => <li key={ m }>{ m }</li>) }</ul>;
export const httpErrorToast = (title, errors) => {
  if (!errors || errors.length === 0) errors = [{}];
  if (!_.isArray(errors)) errors = [errors];

  const messagesSet = new Set(errors.map(e => e.message || 'Unknown error'));
  const messages = Array.from(messagesSet);

  toast({
    type: 'error',
    description: messages.length > 1
      ? renderManyMessages(messages)
      : messages[0],
    title,
  });
};
