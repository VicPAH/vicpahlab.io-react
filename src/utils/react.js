export const getDisplayName = Comp => Comp.displayName || Comp.name || 'Component'
