import _ from 'lodash';
import React, { useCallback, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Button, Dimmer, Form, Loader, Message, Modal } from 'semantic-ui-react';
import {
  CreditCardNumberInput,
  CreditCardExpirationDateInput,
  CreditCardCVVInput,
  CreditCardSubmitButton,
  SquarePaymentForm,
} from 'react-square-payment-form';

import * as a from '../store/actions';
import { httpErrorToast } from '../utils/toast';


/* global SQUARE_APPLICATION_ID */
/* global SQUARE_LOCATION_ID */
/* global SQUARE_SANDBOX */


export const MemberPaymentModal = connect(
  state => ({
    state: state.payMembershipState,
    errors: state.payMembershipErrors,
  }),
  {
    payMembership: a.payMembership,
    clearPayMembershipState: a.clearPayMembershipState,
  },
)(props => {
  const {
    clearPayMembershipState,
    payMembership,

    billingContact,
    errors: payErrors,
    state: payState,
    memberId,
    onClose,
    onComplete,
  } = props;

  useEffect(() => {
    clearPayMembershipState();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const [formLoading, setFormLoading] = useState(true);
  const handleFormLoaded = useCallback(() => { setFormLoading(false) }, [setFormLoading]);

  const [rate, setRate] = useState('full');
  const setRateFull = useCallback(() => setRate('full'), [setRate]);
  const setRateConcession = useCallback(() => setRate('concession'), [setRate]);

  const handleNonceResponse = useCallback((errors, nonce, cardData, token) => {
    if (errors) {
      return;
    }
    payMembership({ rate, nonce, token, key: new Date().toJSON(), id: memberId });
  }, [payMembership, memberId, rate]);
  const handleCreate = useCallback(() => {
    setFormLoading(true);
    return {
      amount: `${rate === 'concession' ? 22 : 37}.00`,
      currencyCode: 'AUD',
      intent: 'CHARGE',
      billingContact,
    };
  }, [billingContact, setFormLoading, rate]);


  useEffect(() => {
    if (!_.isEmpty(payErrors)) {
      httpErrorToast('Payment error', payErrors);
    }
  }, [payErrors]);
  useEffect(() => {
    if (payState === 'success' || payState === 'error') {
      setFormLoading(false);
    }
    if (payState === 'success' && onComplete) {
      onComplete();
    }
  }, [payState]); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <SquarePaymentForm
      sandbox={ SQUARE_SANDBOX }
      applicationId={ SQUARE_APPLICATION_ID }
      locationId={ SQUARE_LOCATION_ID }
      cardNonceResponseReceived={ handleNonceResponse }
      createVerificationDetails={ handleCreate }
      paymentFormLoaded={ handleFormLoaded }
      style={{ width: 'auto' }}
    >
      <Modal { ..._.pick(props, ['open', 'onClose']) } size="tiny">
        <Modal.Header>Pay Membership Dues</Modal.Header>
        <Modal.Content style={{ display: 'relative' }}>
          <Dimmer active={ formLoading } inverted>
            <Loader />
          </Dimmer>
          { !_.isEmpty(payErrors) && (
            <Message error>{ payErrors.map(e => <div key={ e.message }>{ e.message }</div> ) }</Message>
          ) }
          <Form.Field>
            <label>Membership Type</label>
            <div>
              <Button.Group>
                <Button onClick={ setRateFull } active={ rate === 'full' }>Full ($37.00)</Button>
                <Button onClick={ setRateConcession } active={ rate === 'concession' }>Concession ($22.00)</Button>
              </Button.Group>
            </div>
          </Form.Field>
          <CreditCardNumberInput />
          <CreditCardExpirationDateInput />
          <CreditCardCVVInput />
          <CreditCardSubmitButton>
            Pay ${ rate === 'concession' ? 22 : 37 }.00
          </CreditCardSubmitButton>
          <Button
            content="Pay Later"
            icon="close"
            labelPosition="right"
            onClick={ onClose }
            style={{ marginTop: '10px' }}
            negative
            fluid
            />
        </Modal.Content>
      </Modal>
    </SquarePaymentForm>
  );
});
