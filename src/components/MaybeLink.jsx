import path from 'path';
import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'


export class MaybeLink extends PureComponent {
  handleClickDirect(ev) {
    ev.preventDefault()
    document.location = ev.target.href
  }
  render() {
    const { children, to, ...remain } = this.props

    if (!to) {
      return children
    }

    const relUrl = to.startsWith('/') || to.startsWith('.')
    const toUrl = new URL(relUrl ? `${document.location.origin}${to}` : to)
    const isFile = path.extname(to).length > 0;

    if (!isFile) {
      if (relUrl) {
        return <Link to={ to } { ...remain }>{ children }</Link>
      }
      if (toUrl.origin === document.location.origin) {
        return <Link to={ to.substring(document.location.origin.length)} { ...remain }>{ children }</Link>
      }
    }

    return <a href={ to } { ...remain } onClick={ this.handleClickDirect }>{ children }</a>
  }
}
