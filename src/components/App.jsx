import axios from 'axios';
import _ from 'lodash';
import qs from 'query-string';
import React, { Fragment, PureComponent, useEffect } from 'react';
import { connect } from 'react-redux';
import { Redirect, Route, Switch, withRouter } from 'react-router';
import { toast } from 'react-semantic-toasts';
import {
  Dropdown,
  Icon,
  Loader,
  Menu,
  Message,
  Sidebar,
} from 'semantic-ui-react';
import styled, { css, withTheme } from 'styled-components/macro';

import { AdminRouteLoader } from './admin';
import { LoginModal } from './LoginModal';
import { Over18Modal } from './Over18Modal';
import { CommitteePageRoute } from './pages/CommitteePage'
import { ConstitutionPage } from './pages/ConstitutionPage'
import { EventPageRoute } from './pages/EventPage'
import { EventsPage } from './pages/EventsPage'
import { EventGroupPageRoute, primaryEventGroups } from './pages/EventGroupPage'
import { LoadPageRoute } from './pages/LoadPage'
import { MemberPage } from './pages/MemberPage'
import { NewsListPage } from './pages/NewsPage'
import { PhotoComp2020Route } from './pages/PhotoComp2020';
import { PollPageRoute } from './pages/PollPage'
import { NextEventStatisticGroup } from './NextEvent';
import { withTrailingSlash } from './pages/withTrailingSlash';
import { Responsive } from './Responsive';

import fetlifeLogo from '../img/fetlife-logo.svg';
import puppyPrideLogo from '../img/puppypride-logo.svg';

import * as a from '../store/actions';
import * as s from '../store/selectors';


function menuBg(alpha) {
  return `rgba(0,0,0,${alpha})`
}
const bigLogoPx = 115;
const smallLogoPx = 42;
const bigSocialEm = 1.8;
const smallSocialEm = 1.5;
const menuBgLight = menuBg(0.5);
const menuBgDark = menuBg(0.8);
function getScrollState() {
  return {
    logoSize: window.scrollY <= 0 ? bigLogoPx : smallLogoPx,
    menuBg: window.scrollY <= 0 ? menuBgLight : menuBgDark,
    socialSize: window.scrollY <= 0 ? bigSocialEm : smallSocialEm,
  }
}


const TopBar = styled.div`
position: fixed;
z-index: 100;
width: 100%;

& > .ui.menu {
  border-radius: 0;
}
& .ui.menu .ui.dropdown .item {
  color: rgba(255,255,255,0.8) !important;
  &:hover {
    color: rgba(255,255,255,1) !important;
  }
}
`
const BottomBar = styled(TopBar)`
bottom: 0px;
`
const SocialLink = styled.a`
margin-right: ${props => props.theme.spacer}px;
`
const mainHeaderShared = css`
min-height: ${props => props.theme.headerHeight}px;
clip-path: polygon(0 0, 100% 0, 100% ${props => props.theme.headerSlope}%, 0 100%);
`
const MainHeaderOuter = styled.header`
${mainHeaderShared}
background-color: ${props => props.theme.primarydark.toRgbString()};
padding-bottom: ${props => props.theme.spacer}px;

& .ui.statistic {
  margin: 0 !important;
  margin-bottom: 1em !important;
}
`
function headerImageMediaQuery({ previous, current }) {
  let str = '@media ';
  const min = previous && previous.width + 1;
  const max = current.width;
  const minStr = min && `(min-width: ${min}px)`;
  const maxStr = max && `(max-width: ${max}px)`;
  if (min && max) {
    str += `${minStr} and ${maxStr}`;
  } else if (min) {
    str += minStr;
  } else if (max) {
    str += maxStr;
  }
  str += `{ background-image: url(${current.url}); }`;
  return str;
}
function headerImageMediaQueries(props) {
  const cssStr = Object.entries(props.theme.headerImage)
        .map(([width, url]) => ({ width: Number(width), url }))
        .filter(({ width }) => !Number.isNaN(width))
        .sort(({ width: left }, { width: right }) => {
          if (left < right) {
            return -1;
          }
          if (right < left) {
            return 1;
          }
          return 0
        })
        .reduce(
          (acc, current) => ({
            css: `${acc.css}\n${headerImageMediaQuery({ previous: acc.previous, current })}`,
            previous: current,
          }),
          { css: '', previous: null },
        )
        .css;
  return css([cssStr]);
}
// TODO extras
const MainHeaderInner = styled.div`
${mainHeaderShared}
padding-top: ${props => props.theme.headerTopPad}px;
background-image: url(${props => props.theme.headerImage.full});
background-position: center;
background-size: cover;
${props => headerImageMediaQueries(props)}
`

@withRouter
class TopBarItem extends PureComponent {
  getUrl = () => {
    const { href, name } = this.props;
    if (!(href || name)) {
      return;
    }
    return href ? href : `/${name}/`;
  }
  handleClick = ev => {
    const { history } = this.props;
    const url = this.getUrl()
    if (!url) {
      return;
    }
    if (url.startsWith('/')) {
      ev.preventDefault()
      history.push(url)
    }
  }
  render() {
    const { children, dropdown, icon, name, onClick: propOnClick, ...props } = this.props
    const content = name && !children ? name : children;
    const passProps = _.omit(props, ['staticContext'])
    const onClick = propOnClick ? propOnClick : this.handleClick;
    if (dropdown) {
      return (
        <Dropdown.Item { ...passProps } name={ name } onClick={ onClick }>
          { icon && <Icon name={ icon } /> }
          { content }
        </Dropdown.Item>
      )
    } else {
      return (
        <Menu.Item { ...passProps } name={ name } href={ this.getUrl() } onClick={ onClick }>
          { icon && <Icon name={ icon } /> }
          { content }
        </Menu.Item>
      )
    }
  }
}


class MainHeader extends PureComponent {
  render() {
    return (
      <MainHeaderOuter>
        <MainHeaderInner>
          { this.props.children }
        </MainHeaderInner>
      </MainHeaderOuter>
    )
  }
}

@withTheme
class LogoImg extends PureComponent {
  render() {
    const { width, theme } = this.props
    return <img alt="VicPAH" src={ theme.logo } style={{ width }} />
  }
}


const NextEventWrapper = styled.div`
position: absolute;
top: ${props => props.theme.headerTopPad}px;
right: ${props => props.theme.spacer * 5}px;
display: flex;
flex-direction: column;
justify-content: center;

@media (max-width: 550px) {
  position: static;
}
`


const ConstitutionRoute = withTrailingSlash(ConstitutionPage)
const EventsRoute = withTrailingSlash(EventsPage)
const EventGroupRoute = withTrailingSlash(EventGroupPageRoute)
const EventRoute = withTrailingSlash(EventPageRoute)
const NewsListRoute = withTrailingSlash(NewsListPage)

class AppRoutes extends PureComponent {
  state = {
    hasError: false,
  };
  static getDerivedStateFromError() {
    return { hasError: true };
  }
  render() {
    if (this.state.hasError) {
      return <Message error>There was a problem loading the page</Message>
    }
    return (
      <Switch>
        <Route path="/index.html"  render={ () => <Redirect to="/" /> } exact />
        <Route path="/oauth/:service(discord)/callback" render={ this.props.renderOauthCallback } exact />
        <Route path="/association/constitution/"  component={ ConstitutionRoute } exact />
        <Route path="/association/committee/"  component={ CommitteePageRoute } exact />
        <Route path="/association/inclusivity/"  render={ () => <Redirect to="/association/inclusion/" /> } exact />
        <Route path="/picnic/"  render={ () => <Redirect to="/events/2021-04-mosh/" /> } exact />
        <Route path="/pridemarch/" render={ () => <Redirect to="/events/2021-05-midsumma-march/" /> } exact />
        <Route path="/agm/" render={ () => <Redirect to="/association/agm/2021/" /> } exact />
        <Route path={ `/:group(${primaryEventGroups.join('|')})/` } component={ EventGroupRoute } exact />
        <Route path="/events/" component={ EventsRoute } exact />
        <Route path="/events/group/:group/" component={ EventGroupRoute } />
        <Route path="/events/:slug/" component={ EventRoute } />
        <Route path="/members/:slug/" component={ MemberPage } />
        <Route path="/news/" component={ NewsListRoute } exact />
        <Route path="/photo-comp-2020/" component={ PhotoComp2020Route } />
        <Route path="/polls/" component={ PollPageRoute } />
        <Route path="/admin/" component={ AdminRouteLoader } />
        <Route path="/(.*)" component={ LoadPageRoute } />
      </Switch>
    );
  }
}


class TopBarItemsComponent extends PureComponent {
  // adminProfileToggle = () => {
  //   if (localStorage.vicpahMemberProfiles) {
  //     delete localStorage.vicpahMemberProfiles;
  //   } else {
  //     localStorage.vicpahMemberProfiles = true;
  //   }
  //   document.location.reload();
  // }
  render() {
    const { isCommitteeScope, logoSize, menuBg, type } = this.props;
    return (
      <Fragment>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <TopBarItem href="/" name="home" header>
            <LogoImg width={ `${logoSize}px` } />
          </TopBarItem>
        </div>
        <TopBarItem name="events">Events</TopBarItem>
        { type === 'top'
          ? (
            <Dropdown item simple text='Association'>
              <Dropdown.Menu style={{ background: menuBg }}>
                <TopBarItem name="association/membership"
                            dropdown
                            >
                  Membership
                </TopBarItem>
                <TopBarItem name="association/committee"
                            dropdown
                            >
                  Committee
                </TopBarItem>
                <TopBarItem name="association/constitution"
                            dropdown
                            >
                  Constitution
                </TopBarItem>
                <TopBarItem name="association/inclusion"
                            dropdown
                            >
                  Inclusion Statement
                </TopBarItem>
              </Dropdown.Menu>
            </Dropdown>
          )
          : (
            <Menu.Item>
              <Menu.Header>Association</Menu.Header>
              <Menu.Menu>
                <TopBarItem name="association/membership">
                  Membership
                </TopBarItem>
                <TopBarItem name="association/committee">
                  Committee
                </TopBarItem>
                <TopBarItem name="association/constitution">
                  Constitution
                </TopBarItem>
                <TopBarItem name="association/inclusivity">
                  Inclusivity Statement
                </TopBarItem>
              </Menu.Menu>
            </Menu.Item>
          )
        }
        { isCommitteeScope && (
            type === 'top'
              ? (
                <Dropdown item simple text='Committee'>
                  <Dropdown.Menu style={{ background: menuBg }}>
                    <TopBarItem name="admin/emailsig" dropdown>
                      Email Signature
                    </TopBarItem>
                    { isCommitteeScope && (
                      <TopBarItem name="admin/members"
                                  dropdown
                                  >
                        Members
                      </TopBarItem>
                    ) }
                  </Dropdown.Menu>
                </Dropdown>
              )
              : (
                <Menu.Item>
                  <Menu.Header>Committee</Menu.Header>
                  <Menu.Menu>
                    <TopBarItem name="admin/emailsig">
                      Email Signature
                    </TopBarItem>
                    { isCommitteeScope && (
                      <TopBarItem name="admin/members">
                        Members
                      </TopBarItem>
                    ) }
                    {/* <TopBarItem onClick={ this.adminProfileToggle }>
                      Member profiles [{ localStorage.vicpahMemberProfiles ? 'ON' : 'OFF' }]
                    </TopBarItem> */}
                  </Menu.Menu>
                </Menu.Item>
              )
          )
        }
      </Fragment>
    );
  }
}
const TopBarItems = connect(
  state => ({
    isCommitteeScope: s.isCommitteeScope(state),
  }),
)(TopBarItemsComponent)
class Svg extends PureComponent {
  state = {
    svgAttrs: {},
    svgData: null
  }
  componentDidMount() {
    this.loadData();
  }
  componentDidUpdate(prevProps) {
    if (prevProps.src !== this.props.src) {
      this.loadData();
    }
  }
  loadData = async () => {
    const resp = await axios.get(this.props.src);
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(resp.data, 'image/svg+xml');
    const svgEl = xmlDoc.querySelector('svg');
    const svgAttrs = {};
    for (const attr of svgEl.attributes) {
      svgAttrs[attr.name] = attr.value;
    }
    this.setState({
      svgAttrs,
      svgData: svgEl.innerHTML,
    });
  };
  render() {
    const { svgAttrs, svgData } = this.state;
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        { ...svgAttrs }
        { ..._.omit(this.props, ['src']) }
        dangerouslySetInnerHTML={{ __html: svgData }}
        />
    );
  }
}
class SocialItems extends PureComponent {
  render() {
    return (
      <Fragment>
        <SocialLink href="https://www.facebook.com/groups/1529951983945288">
          <Icon name="facebook" />
        </SocialLink>
        <SocialLink href="https://discord.gg/AGUvEmb">
          <Icon name="discord" />
        </SocialLink>
        <SocialLink href="https://twitter.com/vicpah">
          <Icon name="twitter" />
        </SocialLink>
        <SocialLink href="https://www.instagram.com/vicpupsandhandlers/">
          <Icon name="instagram" />
        </SocialLink>
        <SocialLink href="https://www.puppypride.social/m/victorian-pups-and-handlers">
          <Svg
            src={ puppyPrideLogo }
            style={{
              fill: 'white',
              width: '0.8em',
              height: 'auto',
              marginRight: '8px'
            }}
            />
        </SocialLink>
        <SocialLink href="https://fetlife.com/groups/119983">
          <Svg
            src={ fetlifeLogo }
            style={{
              fill: 'white',
              width: '0.8em',
              height: 'auto',
              marginRight: '8px'
            }}
            />
        </SocialLink>
        <SocialLink href="https://t.me/Vicpahevents">
          <Icon name="telegram plane" />
        </SocialLink>
      </Fragment>
    );
  }
}

const OauthErrorHandler = connect(
  state => ({ errors: state.oauthErrors }),
  { setOauthErrors: a.setOauthErrors }
)(props => {
  const { errors, setOauthErrors } = props;
  useEffect(() => {
    if (!errors || errors.length === 0) return;
    for (const err of errors) {
      toast({
        title: 'Oauth error',
        description: err.message,
        type: 'error',
        time: 10000,
      });
    }
    setOauthErrors([]);
  }, [errors, setOauthErrors]);
  return null;
})

const OauthCallback = connect(
  state => ({
    userId: (state.loginPayload || {}).sub,
    oauthState: state.oauthState,
    oauthErrors: state.oauthErrors,
    refreshClient: state.refreshClient,
  }),
  {
    oauth: a.oauth,
    setOauthErrors: a.setOauthErrors,
  },
)(props => {
  const {
    userId, refreshClient, location,
    oauthState,
    match: { params: { service } },
  } = props;
  const { code } = qs.parse(location.search);

  useEffect(() => {
    if (!code || !service) {
      toast({ title: 'Incomplete Oauth request', type: 'error' });
    }
  }, [code, service]);

  useEffect(() => {
    if (!code || !service) return;
    if (userId && !refreshClient) return;
    if (refreshClient) {
      props.oauth({ code, service, client: refreshClient });
    } else {
      props.oauth({ code, service });
    }
    // this effect should only trigger when these props change
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [code, service, refreshClient]);

  if (oauthState !== 'pending') {
    if (!userId) props.showLogin();
    return <Loader active />;
  } else {
    if (userId) {
      return <Redirect to={ `/members/${userId}/discord/` } />
    } else {
      return <Redirect to="/" />
    }
  }
});


@withTheme
@connect(
  state => ({
    loginClient: state.loginClient,
    loggedIn: s.getLoggedIn(state),
  }),
  { logout: a.logout },
)
class App extends PureComponent {
  state = {
    ...getScrollState(),
    sidebarWantVisible: false,
    sidebarIsVisible: false,
    loginVisible: false,
  }
  componentDidMount() {
    document.addEventListener('scroll', this.handleScroll)
  }
  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleScroll)
  }
  handleScroll = ev => {
    this.setState(getScrollState())
  }

  hideSidebar = () => this.setState({ sidebarWantVisible: false })
  showSidebar = () => this.setState({ sidebarWantVisible: true })
  hideSidebarDone = () => this.setState({ sidebarIsVisible: false })
  showSidebarDone = () => this.setState({ sidebarIsVisible: true })
  showLogin = () => this.setState({ loginVisible: true });
  hideLogin = () => this.setState({
    loginVisible: false,
  });
  handleLogout = () => this.props.logout();

  renderOauthCallback = props => {
    return <OauthCallback showLogin={ this.showLogin } { ...props } />
  }

  render() {
    const { loggedIn, loginClient, theme } = this.props;
    const {
      loginVisible,
      logoSize, menuBg, sidebarIsVisible, sidebarWantVisible, /*socialSize*/
    } = this.state;

    return (
      <Fragment>
        { !loginClient && <Over18Modal /> }
        <OauthErrorHandler />
        <Sidebar.Pushable
          style={{
            position: 'fixed',
            width: sidebarWantVisible || sidebarIsVisible ? '100%' : 0,
            height: sidebarWantVisible || sidebarIsVisible ? '100%' : 0,
            zIndex: 10000,
          }}
          >
          <Sidebar
            as={ Menu }
            animation='overlay'
            icon='labeled'
            inverted
            onHide={ this.hideSidebar }
            onHidden={ this.hideSidebarDone }
            onShow={ this.showSidebarDone }
            vertical
            visible={ sidebarWantVisible }
            width="wide"
            >
            <TopBarItems logoSize={ bigLogoPx } menuBg={ menuBg } type="side" />
          </Sidebar>
          <Sidebar.Pusher dimmed={ sidebarWantVisible }>
            <div style={{ height: '100%', width: '100%' }}></div>
          </Sidebar.Pusher>
        </Sidebar.Pushable>
        <LoginModal
          open={ loginVisible && !loginClient }
          onClose={ this.hideLogin }
        />
        <TopBar>
          <Menu style={{ width: '100%', background: menuBg }}
                borderless
                inverted
                >
            <Responsive as="span" maxWidth={ theme.phoneWidth }>
              <div style={{ position: 'fixed', top: '0px', display: 'flex', justifyContent: 'center', width: '100%' }}>
                <TopBarItem href="/" name="home" header>
                  <LogoImg width={ `${logoSize}px` } />
                </TopBarItem>
              </div>
              <TopBarItem onClick={ this.showSidebar }><Icon name="bars" /></TopBarItem>
            </Responsive>
            <Responsive as="span" minWidth={ theme.phoneWidth + 1 }>
              <TopBarItems logoSize={ logoSize } menuBg={ menuBg } type="top" />
            </Responsive>
            { !loginClient && (
              <Menu.Item onClick={ this.showLogin } position="right">
                Login
              </Menu.Item>
            ) }
            { loginClient && !loggedIn && (
              <Menu.Item position="right">
                <Loader active inline />
              </Menu.Item>
            ) }
            { loggedIn && (
              <Menu.Menu position="right">
                <Dropdown item simple text={ loggedIn.displayName || loggedIn.email }>
                  <Dropdown.Menu style={{ background: menuBg }}>
                    <TopBarItem name={ `members/${loggedIn.id}` }
                                dropdown
                                >
                      Profile
                    </TopBarItem>
                    <TopBarItem name="logout"
                                dropdown
                                onClick={ this.handleLogout }
                                >
                      Logout
                    </TopBarItem>
                  </Dropdown.Menu>
                </Dropdown>
              </Menu.Menu>
            ) }
          </Menu>
        </TopBar>
        <MainHeader>
          <NextEventWrapper>
            <NextEventStatisticGroup urls={[
                                       "/content/events/groups/vicpah/index.ics",
                                       "/content/events/groups/major/index.ics",
                                     ]}
                                     size="small"
                                     inverted
                                     />
          </NextEventWrapper>
        </MainHeader>
        <AppRoutes renderOauthCallback={ this.renderOauthCallback } />
        <div style={{ height: '70px' }} />
        <BottomBar>
          <Menu style={{ width: '100%', background: menuBgLight }}
                borderless
                inverted
                >
            <Responsive as="span" maxWidth={ theme.phoneWidth }>
              <TopBarItem as="span" header>
                <SocialItems />
              </TopBarItem>
            </Responsive>
            <Responsive as="span" minWidth={ theme.phoneWidth + 1 }>
              <TopBarItem as="span" style={{ fontSize: `${smallSocialEm}em` }}header>
                <SocialItems />
              </TopBarItem>
            </Responsive>
          </Menu>
        </BottomBar>
      </Fragment>
    )
  }
}

export { App };
