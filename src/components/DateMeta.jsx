import React, { PureComponent } from 'react';
import { List, Icon } from 'semantic-ui-react';

import { LiveFromNow } from './LiveTime';
import { timezoneToText } from '../utils/ical';


export class DateMeta extends PureComponent {
  render() {
    const { date, timezone } = this.props

    return (
      <List horizontal>
        <List.Item>
          <Icon name="clock outline" />&nbsp;
          <LiveFromNow dt={ date } interval={ 10000 }/>
        </List.Item>
        <List.Item>
          <Icon name="map outline" />&nbsp;
          { timezoneToText(timezone) }
        </List.Item>
      </List>
    )
  }
}
