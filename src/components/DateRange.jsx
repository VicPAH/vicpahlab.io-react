import React, { PureComponent } from 'react';
import moment from 'moment-timezone';
import { timezoneToText } from '../utils/ical';


export class DateRange extends PureComponent {
  static defaultProps = {
    showstartday: true,
    showendday: 'dynamic',
    showstartmonth: true,
    showendmonth: 'dynamic',
    showtimezone: true,
  }
  dateFs = (prefix, dynamicValue=false) => {
    const {
      [`show${prefix}day`]: showDay,
      [`show${prefix}month`]: showMonth,
    } = this.props

    const fsParts = []
    if (showDay === true || (showMonth === 'dynamic' && dynamicValue)) {
      fsParts.push('Do')
    }
    if (showMonth === true || (showMonth === 'dynamic' && dynamicValue)) {
      fsParts.push('MMMM')
    }
    fsParts.push('HH:mm')

    return fsParts.join(' ')
  }
  render() {
    const {
      start: startRaw,
      end: endRaw,
      timezone: timezoneRaw,
      showtimezone
    } = this.props

    const start = moment.isMoment(startRaw)
          ? startRaw
          : moment.tz(startRaw, timezoneRaw)
    const end = moment.isMoment(endRaw)
          ? endRaw
          : moment.tz(endRaw, timezoneRaw)
    const startEndOfDay = start.clone().endOf('day')

    const startFs = this.dateFs('start')
    const endFs = this.dateFs('end', end.isAfter(startEndOfDay))

    return <span>
      { start.format(startFs) } - { end.format(endFs) }
      { showtimezone && timezoneRaw && ` (${timezoneToText(timezoneRaw)})` }
    </span>
  }
}
