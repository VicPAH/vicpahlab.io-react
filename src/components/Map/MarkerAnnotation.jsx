import { PureComponent } from 'react';

import { withMap } from './Map';
import { withPoint } from './Point';

import 'apple-mapkit-js'
/* global mapkit */


@withMap
@withPoint()
class MarkerAnnotation extends PureComponent {
  state = {
    marker: null,
  }
  componentDidMount() {
    this.updateMap();
  }
  componentDidUpdate(prevProps) {
    if (this.props.map !== prevProps.map) {
      this.updateMap()
    }
  }
  componentWillUnmount() {
    const { map } = this.props;
    const { marker } = this.state;
    if (map && marker) {
      map.removeAnnotation(marker);
    }
  }
  updateMap = () => {
    const { point, map, ...opts } = this.props;
    const { marker } = this.state;
    if (marker) {
      map.removeAnnotation(marker);
    }
    const newMarker = new mapkit.MarkerAnnotation(point, opts);
    this.setState({ marker: newMarker });
    map.addAnnotation(newMarker);
  }
  render() {
    return null;
  }
}
export { MarkerAnnotation };
