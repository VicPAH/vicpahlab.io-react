import React, { Component, createContext, PureComponent } from 'react';

import { withMap } from './Map';
import { getDisplayName } from '../../utils/react';

import 'apple-mapkit-js'
/* global mapkit */


const PlaceCtx = createContext(null);
export const PlaceCtxProvider = ({ children, value, ...props }) => (
  <PlaceCtx.Provider value={ value }>
    <PointCtx.Provider value={ value ? value.coordinate : null }>
      { children }
    </PointCtx.Provider>
  </PlaceCtx.Provider>
)
export const withPlace = ({ alwaysRender = false } = {}) => WrappedComponent => class WithPlace extends Component {
  static contextType = PlaceCtx;
  static displayName = `withPlace(${getDisplayName(WrappedComponent)})`;
  render() {
    if (alwaysRender || this.context) {
      return <WrappedComponent place={ this.context } { ...this.props } />;
    }
    return null;
  }
}


export const PointCtx = createContext(null);
export const withPoint = ({ alwaysRender = false } = {}) => WrappedComponent => class WithPoint extends Component {
  static contextType = PointCtx;
  static displayName = `withPoint(${getDisplayName(WrappedComponent)})`;
  render() {
    if (alwaysRender || this.context) {
      return <WrappedComponent point={ this.context } { ...this.props } />;
    }
    return null;
  }
}


@withMap
@withPoint()
class SetRegion extends PureComponent {
  componentDidMount() {
    this.setRegion();
  }
  componentDidUpdate(prevProps) {
    if (prevProps.point !== this.props.point) {
      this.setRegion();
    }
  }
  setRegion() {
    const { map, point } = this.props;
    map.setRegionAnimated(new mapkit.CoordinateRegion(
      point,
      new mapkit.CoordinateSpan(0.1, 0.01),
    ));
  }
  render() { return null; }
}
export { SetRegion };
