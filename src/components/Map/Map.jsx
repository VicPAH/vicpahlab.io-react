import axios from 'axios';
import _ from 'lodash';
import React, { Component, createContext, PureComponent } from 'react';
import styled from 'styled-components/macro';

import { getDisplayName } from '../../utils/react';

import 'apple-mapkit-js'
/* global mapkit */
/* global MAPKIT_JWT_URL */


const InitStates = Object.freeze({
  notStarted: 1,
  started: 2,
  complete: 3,
})
let mapkitState = InitStates.notStarted;


export const withMapkit = WrappedComponent => class WithMapkit extends Component {
  static displayName = `withMapkit(${getDisplayName(WrappedComponent)})`;
  state = {
    mapkitState: null,
  }
  authorizationCb = async cb => {
    let resp = null;
    try {
      resp = await axios.post(MAPKIT_JWT_URL + '/create_token');
    } catch (err) {
      console.error('Mapkit JWT error:', err)
      setTimeout(() => this.authorizationCb(cb), 5000);
      return;
    }
    cb(resp.data.token);
  }
  componentDidMount() {
    this.setState({ mapkitState });
    if (mapkitState !== InitStates.complete) {
      mapkit.addEventListener(
        'configuration-change',
        event => {
          if (event.status === 'Initialized') {
            mapkitState = InitStates.complete;
            this.setState({ mapkitState });
          }
        },
      );
    }
    if (mapkitState === InitStates.notStarted) {
      mapkitState = InitStates.started;
      mapkit.init({
        authorizationCallback: this.authorizationCb,
      });
    }
  }
  render() {
    if (mapkitState === InitStates.complete) {
      return <WrappedComponent { ...this.props } />;
    }
    return null;
  }
}


const MapCtx = createContext(null);
export const withMap = WrappedComponent => class WithMap extends Component {
  static contextType = MapCtx;
  static displayName = `withMap(${getDisplayName(WrappedComponent)})`;
  render() {
    return <WrappedComponent map={ this.context } { ...this.props } />;
  }
}


const MapWrapper = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};
  position: relative;
`


const wrapMapFn = fn => function(...props) {
  if (this._destroyed) {
    return;
  }
  return fn.apply(this, props);
}


@withMapkit
class Map extends PureComponent {
  state = {
    map: null,
  }
  setWrapperRef = el => {
    const {
      interactive,
      onRegionChangeEnd,
      onRegionChangeStart,
    } = this.props;
    const { map } = this.state;
    if (map) {
      map.destroy();
    }
    if (!el) {
      return;
    }

    const opts = {};

    if (!interactive) {
      Object.assign(opts, {
        isRotationEnabled: false,
        isScrollEnabled: false,
        isZoomEnabled: false,
      });
    }

    const newMap = new mapkit.Map(el, opts);
    const destroy = newMap.destroy;
    const component = this;
    newMap._destroyed = false;
    newMap.destroy = wrapMapFn(function() {
      newMap._destroyed = true;
      component.setState({ map: null });
      destroy.apply(newMap);
    })
    newMap.addAnnotation = wrapMapFn(newMap.addAnnotation);
    newMap.removeAnnotation = wrapMapFn(newMap.removeAnnotation);
    newMap.setRegionAnimated = wrapMapFn(newMap.setRegionAnimated);

    if (onRegionChangeEnd) { newMap.addEventListener('region-change-end', onRegionChangeEnd) }
    if (onRegionChangeStart) { newMap.addEventListener('region-change-start', onRegionChangeStart) }

    this.setState({ map: newMap })
  }
  componentWillUnmount() {
    const { map } = this.state;
    if (map) {
      map.destroy();
    }
  }
  render() {
    const { children } = this.props;
    return (
      <MapCtx.Provider value={ this.state.map }>
        <MapWrapper { ..._.pick(this.props, ['width', 'height']) }
                    ref={ this.setWrapperRef }
                    >
          { children }
        </MapWrapper>
      </MapCtx.Provider>
    );
  }
}
export { Map }
