import _ from 'lodash';
import React, { PureComponent } from 'react';

import { PlaceCtxProvider } from './Point';
import { withMapkit } from './Map';

import 'apple-mapkit-js'
/* global mapkit */


@withMapkit
class GeocodedAddress extends PureComponent {
  state = {
    point: null,
  };
  componentDidMount() {
    this.geocodeAddress();
  }
  componentDidUpdate(prevProps) {
    if (this.props.address !== prevProps.address) {
      this.geocodeAddress();
    }
  }
  geocodeAddress = () => {
    const { address } = this.props;
    const geocoder = new mapkit.Geocoder({});
    geocoder.lookup(address, (err, value) => {
      console.log({ err,  value})
      if (err) {
        console.error('Error geocoding address:', err);
        return;
      }
      if (_.isEmpty(value.results)) {
        console.error('No results');
        return;
      }
      this.setState({ point: value.results[0] });
    })
  }
  render() {
    const { children } = this.props;
    const { point } = this.state;
    return <PlaceCtxProvider value={ point }>{ children }</PlaceCtxProvider>;
  }
}
export { GeocodedAddress }
