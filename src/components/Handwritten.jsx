import React, { useEffect } from 'react';
import styled from 'styled-components/macro';


const weight = 700;
const HandwrittenWrapper = styled.span`
  font-family: Caveat, cursive;
  font-weight: ${weight};
`;

export const Handwritten = props => {
  useEffect(() => {
    import(`@fontsource/caveat/${weight}.css`);
  }, []);
  return <HandwrittenWrapper { ...props } />;
};
