import axios from 'axios';
import _ from 'lodash';
import moment from 'moment-timezone';
import React, { Fragment, PureComponent } from 'react';
import { Message, Statistic } from 'semantic-ui-react';

import { LiveFromNow } from './LiveTime';
import { MaybeLink } from './MaybeLink';
import { Calendar, timezoneToText } from '../utils/ical';

class NextEventStatisticComponent extends PureComponent {
  render() {
    const { event, ...props } = this.props
    if (!event) {
      return null
    }

    const {
      dtstart,
      dtstartTz = 'australia/melbourne',
      summary,
      url,
    } = event._getProps()

    return (
      <Statistic { ...props }>
        <Statistic.Value>
          <LiveFromNow dt={ dtstart } withSuffix={ false } interval={ 10000 } />
        </Statistic.Value>
        <Statistic.Label>
          until <MaybeLink to={ url }>{ summary }</MaybeLink><br />
          { dtstartTz.toLowerCase() !== 'australia/melbourne'
            && timezoneToText(dtstartTz)
          }
        </Statistic.Label>
      </Statistic>
    )
  }
}


export class NextEventStatisticGroup extends PureComponent {
  state = {
    hasError: false,
    eventsList: [],
    eventsMap: {},
    requests: {},
  }
  static getDerivedStateFromError() {
    return { hasError: true };
  }
  componentDidMount() {
    this.loadUrls(this.props.urls)
  }
  componentDidUpdate(prevProps) {
    if (!_.isEqual(this.props.urls, prevProps.urls)) {
      this.loadUrls(this.props.urls)
    }
  }
  loadUrls = urls => {
    for (const { cancel } of Object.values(this.state.requests)) {
      cancel.cancel();
    }

    this.setState({
      eventsList: [],
      eventsMap: {},
      allEventsMap: {},
    })

    const requests = _.fromPairs(urls.map(url => {
      if (this.state.requests[url]) {
        return [url, this.state.requests[url]]
      }
      const cancel = axios.CancelToken.source();
      const promise = axios
            .get(
              url,
              {
                responseType: 'text',
                cancelToken: cancel.token,
              },
            )
            .then(resp => {
              const cal = Calendar.fromString(resp.data)
              const events = cal.getEvents({ sortBy: 'dtstart' })
              this.setAllEvents(url, events)
              this.refreshEventsList()
            })
      return [url, { cancel, promise }]
    }))

    this.setState({ requests })
  }
  setAllEvents = (url, events) => {
    this.setState({ allEventsMap: {
      ...this.state.allEventsMap,
      [url]: events,
    } });
  }
  refreshEventsList = () => {
    const { allEventsMap, refreshTimeout } = this.state;
    const now = moment();
    const eventsMap = _.chain(this.props.urls)
          .map(url => [
            url,
            (allEventsMap[url] || [])
              .find(event => event._getProps().dtstart.isAfter(now)),
          ])
          .fromPairs()
          .value();
    const eventsList = _.chain(this.props.urls)
          .map(iUrl => eventsMap[iUrl])
          .filter(Boolean)
          .uniqBy(iEvent => iEvent.getUid())
          .value();

    this.setState({ eventsMap, eventsList });

    const wait = Math.min(...eventsList.map(event => event._getProps().dtstart.diff(now)));
    if (refreshTimeout) {
      clearTimeout(refreshTimeout);
    }
    if (wait) {
      this.setState({
        refreshTimeout: setTimeout(this.refreshEventsList, wait),
      });
    }
  }
  render() {
    if (this.state.hasError) {
      return <Message error>Error loading next events</Message>
    }
    return (
      <Fragment>
        { this.state.eventsList.map(event => (
          <NextEventStatisticComponent key={ event.getUid() }
                                       event={ event }
                                       { ..._.omit(this.props, ['urls']) }
                                       />
        )) }
      </Fragment>
    )
  }
}
export class NextEventStatistic extends PureComponent {
  render() {
    const { url, ...props } = this.props
    return <NextEventStatisticGroup urls={[ url ]} { ...props } />
  }
}
