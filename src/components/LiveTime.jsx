import _ from 'lodash'
import moment from 'moment-timezone';
import React, { Component } from 'react'



export class LiveFromNow extends Component {
  static defaultProps = {
    withSuffix: true,
    accurate: false,
  }
  isAccurate = () => {
    const { accurate, dt } = this.props
    if (accurate === true || accurate === false) {
      return accurate
    }
    return moment().isAfter(dt.clone().subtract(accurate))
  }
  tickRender = () => (
    this.isAccurate()
      ? (
        <AccurateHumanDuration
          withSuffix={ this.props.withSuffix }
          duration={ this.props.dt.diff(moment()) }
          />
      )
      : this.props.dt.fromNow(!this.props.withSuffix)
  )
  render() {
    return (
      <LiveTime
        render={ this.tickRender }
        {..._.omit(this.props, ['withSuffix', 'accurate']) }
        />
    )
  }
}

export class AccurateHumanDuration extends Component {
  static defaultProps = {
    withSuffix: true,
  }
  render() {
    const { duration: rawDuration, withSuffix } = this.props
    const dur = moment.isDuration(rawDuration)
          ? rawDuration
          : moment.duration(rawDuration)

    const pos = dur.asMilliseconds() >= 0
    const timeStr = [
        Math.abs(dur.hours()),
      _.padStart(Math.abs(dur.minutes()), 2, '0'),
      _.padStart(Math.abs(dur.seconds()), 2, '0'),
    ].join(':')
    const days = Math.floor(Math.abs(dur.asDays()))
    const daysStr = days > 0 && `${days} day${days > 1 && 's'}`

    return [
      withSuffix && pos && 'in',
      daysStr,
      timeStr,
      withSuffix && !pos && 'ago',
    ].filter(Boolean).join(' ')
  }
}


export class LiveTime extends Component {
  componentDidMount() {
    this.setup()
  }
  componentDidUpdate(prevProps) {
    if (prevProps.interval !== this.props.interval) {
      this.teardown()
      this.setup()
    }
  }
  componentWillUnmount() {
    this.teardown()
  }
  setup = () => {
    if (this.props.interval) {
      this.setState({
        interval: setInterval(this.tick, this.props.interval),
      })
    }
  }
  teardown = () => {
    clearInterval(this.state.interval)
  }
  tick = () => {
    this.setState({});
  }
  render() {
    return this.props.render();
  }
}
