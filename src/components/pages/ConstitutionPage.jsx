import React, { PureComponent } from 'react';
import { Container } from 'semantic-ui-react';


class EmbedPage extends PureComponent {
  state = {
    height: 600,
  }
  setHeight = doc => {
    this.setState({ height: doc.body.scrollHeight })
  }
  setFrameRef = el => {
    if (!el) {
      return
    }
    el.contentWindow.addEventListener('load', ev => {
      this.setHeight(ev.target)
      const doc = ev.target
      for (const linkEl of doc.getElementsByTagName('a')) {  // eslint-disable-line no-unused-vars
        if (linkEl.href) {
          const hrefUrl = new URL(linkEl.href)
          const targetEls = hrefUrl.hash
                ? doc.getElementsByName(hrefUrl.hash.substr(1))
                : []
          if (targetEls.length) {
            linkEl.addEventListener('click', ev => {
              const iframeTop = el.getBoundingClientRect().top
              const topBarHeight = 70; // document.body.getElementsByClassName('topBar')[0].clientHeight
              const padding = 16
              window.scrollBy(0, targetEls[0].getBoundingClientRect().top + iframeTop - topBarHeight - padding)
            })
          } else {
            linkEl.setAttribute('target', '_parent')
          }
        }
      }
    })
  }
  render() {
    return (
      <iframe
        src={ this.props.src }
        title="Embedded content"
        style={{
          width: '100%',
          height: `${this.state.height}px`,
          border: 'none',
        }}
        ref={ this.setFrameRef }
      />
    )
  }
}

export class ConstitutionPage extends PureComponent {
  render() {
    return (
      <Container className="content" text>
        <h1>VicPAH Constitution</h1>
        <EmbedPage src="/content/association/constitution/raw.html" />
      </Container>
    );
  }
}
