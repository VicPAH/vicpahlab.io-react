import React, { PureComponent } from 'react';

import { EventGroupPage } from './EventGroupPage'


export class EventsPage extends PureComponent {
  render() {
    return <EventGroupPage group="index" />
  }
}
