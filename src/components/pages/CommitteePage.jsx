import _ from 'lodash';
import React, { Fragment, PureComponent } from 'react';
import { Card, Icon, Image } from 'semantic-ui-react';
import styled from 'styled-components/macro';

import { createLoadPage, createLoadPageRoute } from './LoadPage';
import { MarkdownPage } from './MarkdownPage';


const HeadshotIcon = styled(Icon)`
font-size: 240px !important;
margin-top: -40px !important;
opacity: 20% !important;
`
const HeadshotSpacer = styled.div`
height: 90px;
`

const HeadshotPlaceholder = props => (
  <Fragment>
    <HeadshotIcon name="paw" />
    <HeadshotSpacer />
  </Fragment>
)


class CommitteePageComponent extends PureComponent {
  state = {
    members: [],
  }
  handleMeta = meta => {
    this.setState(_.pick(meta, ['members']));
  }
  render() {
    const { response } = this.props
    const { members } = this.state
    return (
      <Fragment>
        <MarkdownPage response={ response } onMeta={ this.handleMeta } />
        <Card.Group>
          { members.map(({name, email, prefix, role, image}) => (
            <Card key={ name }>
              { image
                ? <Image src={ `/content/association/committee/${image}` } wrapped ui={ false } />
                : <HeadshotPlaceholder />
              }
              <Card.Content>
                <Card.Header>{ name }</Card.Header>
                { role && <Card.Meta>{ role }</Card.Meta> }
                { prefix && <Card.Meta>{ prefix }</Card.Meta> }
              </Card.Content>
              { email && (
                <Card.Content extra>
                  <a href={`mailto:${email}`}>
                    <Icon name="mail" /> { email }
                  </a>
                </Card.Content>
              ) }
            </Card>
          )) }
        </Card.Group>
      </Fragment>
    )
  }
}

export const CommitteePage = createLoadPage(CommitteePageComponent)
export const CommitteePageRoute = createLoadPageRoute(CommitteePage)
