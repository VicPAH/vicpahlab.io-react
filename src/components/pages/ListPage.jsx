import axios from 'axios';
import _ from 'lodash';
import path from 'path';
import React, { PureComponent } from 'react';

import { getDisplayName } from '../../utils/react';

export const withDirectoryList = WrappedComponent => class WithDirectoryList extends PureComponent {
  static displayName = `withDirectoryList(${getDisplayName(WrappedComponent)})`
  state = {
    data: null,
    error: null,
    loading: false,
  }
  componentDidMount() {
    this.load(this.getUrl(this.props))
  }
  componentDidUpdate(prevProps) {
    const thisUrl = this.getUrl(this.props)
    const prevUrl = this.getUrl(prevProps)
    if (thisUrl !== prevUrl) {
      this.load(thisUrl)
    }
  }
  getUrl = props => {
    if (props.url) {
      return props.url;
    }
    if (props.match) {
      return props.match.url;
    }
    throw new Error('Unknown URL parameter');
  }
  load = async url => {
    if (this.state.cancel) {
      this.state.cancel.cancel();
    }

    const cancel = axios.CancelToken.source();
    this.setState({ loading: true, data: null, error: null, cancel })

    if (!url.endsWith('/')) {
      url = path.dirname(url) + '/'
    }
    url = `/content${url}index.json`

    if (url.startsWith('/')) {
      url = document.location.origin + url
    }

    let response
    try {
      response = await axios.get(url, { cancelToken: cancel.token });
    } catch(error) {
      if (axios.isCancel(error)) {
        return;
      }
      this.setState({ loading: false, data: null, cancel: null, error });
      return;
    }
    this.setState({ loading: false, cancel: null, error: null, data: response.data });
  }
  render() {
    return (
      <WrappedComponent
        { ...this.props }
        { ..._.chain(this.state)
          .omit(['cancel'])
          .mapKeys((v, k) => _.camelCase(`list ${k}`))
          .value()
        }
        />
    )
  }
}
