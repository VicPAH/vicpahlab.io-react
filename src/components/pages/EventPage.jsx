import _ from 'lodash';
import moment from 'moment-timezone';
import React, { Component, Fragment, PureComponent } from 'react';
import { Button, Icon } from 'semantic-ui-react';
import styled from 'styled-components/macro';

import { Comments } from '../Comments';
import { DateMeta } from '../DateMeta';
import { DateRange } from '../DateRange';
import { createLoadPage, createLoadPageRoute } from './LoadPage';
import { LiveFromNow } from '../LiveTime';
import { GeocodedAddress, Map, MarkerAnnotation, SetRegion } from '../Map';
import { MarkdownPage } from './MarkdownPage';
import { MaybeLink } from '../MaybeLink';
import { MetaRow, TitleRow } from '../Rail';
import { getMarkdownToReact } from '../../utils/markdown';



const MoshMunchRow = styled.p`
font-weight: bold;
font-size: 1.1em;
padding: 0;
padding-top: 1em;
`
const Spacer = styled.div`
height: ${props => props.theme.spacer * 3}px;
`

const AddressRowWrapper = styled.div`
display: flex;
`
const AddressColWrapper = styled.div`
white-space: nowrap;
padding-right: ${props => props.theme.spacer * 2}px;
`
const AddressMapWrapper = styled.div`
position: relative;
width: 100%;
`
const CounterWrapper = styled.div`
font-size: 2em;
font-weight: bold;
text-align: center;
`
const EndedWrapper = styled.div`
font-weight: bold;
text-align: center;
`


const EventGroupPlural = props => React.Children.map(
  props.children,
  child => {
    switch(child) {
    case 'netflix':
      return 'Netflix parties'
    case 'hangout':
      return 'Hangouts'
    default:
      return `${child}es`
    }
  }
)


class EventPageComponent extends PureComponent {
  state = {
    meta: null,
  }
  render() {
    return <EventPageContent { ...this.props } { ...this.state } />
  }
}

class EventPageContent extends Component {
  state = {
    meta: null,
  }
  handleMeta = meta => {
    for (const key of ['cost', 'dressCode', 'pupInfo']) {
      if (!meta[key]) {
        continue
      }
      getMarkdownToReact().process(
        meta[key],
        (err, file) => this.setState({ [`${key}El`]: file.contents })
      )
    }

    const now = moment();
    const start = this.getMoment('start', meta);
    const end = this.getMoment('end', meta);

    this.setState({
      meta,
      startTimeout: setTimeout(() => this.setState({}), start.diff(now)),
      endTimeout: setTimeout(() => this.setState({}), end.diff(now)),
    })
  }
  componentWillUnmount() {
    if (this.state.startTimeout)
      clearTimeout(this.state.startTimeout)
    if (this.state.endTimeout)
      clearTimeout(this.state.endTimeout)
  }
  renderAddressLines(adr) {
    const lines = [
      adr.street_address,
      adr.suburb,
    ]
    if (adr.name) {
      lines.unshift(adr.name)
    }
    if (adr.state) {
      lines.push(adr.state)
    }
    if (adr.country) {
      lines.push(adr.country)
    }
    return lines;
  }
  renderAddress(adr) {
    return (
      <Fragment>
        { this.renderAddressLines(adr).map(
          line => <Fragment key={ line }>{ line }<br /></Fragment>
        ) }
      </Fragment>
    )
  }
  renderTryBooking = eid => (
    <Button primary icon labelPosition="left" onClick={ () => this.openTryBooking(eid) }>
      <Icon name="ticket" />
      Book Now
    </Button>
  )
  openTryBooking = eid => {
    window.open(
      `https://www.trybooking.com/book/sessions?embed&eid=${eid}`,
      'trybooking',
      'menubar=0,scrollbars=1,width=800,height=700',
    );
  }

  getMoment = (k, metaOverride=null) => {
    const { meta: metaRaw } = this.state;
    const meta = metaOverride || metaRaw || {};
    if (meta[k]) {
      return this.toMoment(
        meta[k],
        meta.timezone || 'Australia/Melbourne'
      );
    }
    return null;
  }
  toMoment = (dt, tz) => moment.tz(
    dt.toJSON
      ? _.trimEnd(dt.toJSON(), 'Z')
      : dt,
    tz,
  )

  render() {
    const { response } = this.props
    const { meta, costEl, dressCodeEl, pupInfoEl } = this.state

    const timezone = (meta && meta.timezone) || 'Australia/Melbourne'
    const eventGroups = (meta && meta.eventGroups) || []

    const momentStart = this.getMoment('start')
    const momentEnd = this.getMoment('end')
    const before = momentStart && moment().isBefore(momentStart)
    const after = momentEnd && moment().isAfter(momentEnd)
    const during = (
      momentStart &&
        momentEnd &&
        moment().isAfter(momentStart) &&
        moment().isBefore(momentEnd)
    )
    const withCountdown = eventGroups.indexOf('netflix') !== -1
    const withCountup = withCountdown
    const withEnded = withCountdown

    return (
      <Fragment>
        { meta && (
          <Fragment>
            <TitleRow railContent={ <MaybeLink to="/events/"><Icon name="angle left" /> All events</MaybeLink> }>
              <h1>{ meta.title }</h1>
            </TitleRow>
            <MetaRow icon="calendar">
              <div>
                <DateRange start={ momentStart }
                           end={ momentEnd }
                           showTimezone={ false }
                           />
              </div>
              <div>
                <DateMeta date={ momentStart }
                          timezone={ timezone }
                          />
              </div>
            </MetaRow>
            { (
              (withCountdown && before) ||
                (withCountup && during) ||
                (withEnded && after)
              ) && (
                <MetaRow icon="clock">
                  { withCountdown && before && (
                    <CounterWrapper>
                      Starting <LiveFromNow dt={ momentStart } accurate={{ minutes: 5 }} interval={ 1000 } />
                    </CounterWrapper>
                  ) }
                  { withCountup && during && (
                    <CounterWrapper>
                      Started <LiveFromNow dt={ momentStart } accurate={ true } interval={ 1000 } />
                    </CounterWrapper>
                  ) }
                  { withEnded && after && <EndedWrapper>Event is over</EndedWrapper> }
                </MetaRow>
              )
            }
            { meta.adr && (
              <MetaRow icon="compass">
                <AddressRowWrapper>
                  <AddressColWrapper>
                    { this.renderAddress(meta.adr) }
                  </AddressColWrapper>
                  <AddressMapWrapper>
                    <Map width="100%" height="200px">
                      <GeocodedAddress address={ this.renderAddressLines(meta.adr).join(', ') }>
                        <MarkerAnnotation />
                        <SetRegion />
                      </GeocodedAddress>
                    </Map>
                  </AddressMapWrapper>
                </AddressRowWrapper>
              </MetaRow>
            ) }
            { meta.pollId && meta.pollLabel && (
              <MetaRow icon="chart bar" label="Poll">
                <MaybeLink to={ `/polls/${meta.pollId}` }>{ meta.pollLabel }</MaybeLink>
              </MetaRow>
            ) }
            { costEl && (
              <MetaRow icon="dollar" label="Cost">
                { costEl }
                { meta.tryBookingEid && (
                  <div style={{ paddingTop: '20px' }}>
                    { this.renderTryBooking(meta.tryBookingEid) }
                  </div>
                ) }
              </MetaRow>
            ) }
            { dressCodeEl && <MetaRow icon="user" label="Dress code">{ dressCodeEl }</MetaRow> }
            { pupInfoEl && <MetaRow icon="paw" label="Extra info for pups">{ pupInfoEl }</MetaRow> }
          </Fragment>
        ) }
        <MarkdownPage response={ response } onMeta={ this.handleMeta } />
        { ['mosh', 'munch', 'netflix', 'hangout'].map(key => eventGroups.indexOf(key) >= 0 && (
          <MoshMunchRow key={ key }>
            <MaybeLink to={ `/events/group/${key}` }>
              General information about regular <EventGroupPlural>{ key }</EventGroupPlural>
            </MaybeLink>
          </MoshMunchRow>
        )) }
        { meta && (meta.link || meta.facebook) && (
          <MetaRow icon="linkify" label="More info" noDivider>
            { meta.link && <div><MaybeLink to={ meta.link }>{ meta.link }</MaybeLink></div> }
            { meta.facebook && <div><MaybeLink to={ meta.facebook }>{ meta.facebook }</MaybeLink></div> }
          </MetaRow>
        ) }
        <Spacer />
        <Comments />
      </Fragment>
    )
  }
}

export const EventPage = createLoadPage(EventPageComponent)
export const EventPageRoute = createLoadPageRoute(EventPage)
