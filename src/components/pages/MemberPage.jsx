import _ from 'lodash';
import React, { Fragment, useCallback, useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router';
import { toast } from 'react-semantic-toasts';
import { Button, Container, Divider, Form, Icon, Label, Loader, Message, Radio, Step, Tab } from 'semantic-ui-react';
import styled from 'styled-components/macro';

import { MemberApplicationModal } from '../MemberApplicationModal';
import { MemberPaymentModal } from '../MemberPaymentModal';
import { stateCan, stateLogToMemberSvc } from '../../utils/memberSvc';

import * as a from '../../store/actions';
import * as s from '../../store/selectors';

/* global ENV */
/* global DISCORD_OAUTH_URL */
/* global DISCORD_ADMIN_OAUTH_URL */

const BottomFixed = styled.div`
position: fixed;
bottom: 70px;
left: 0px;
width: 100%;
`;
const MessageWrapper = styled.div`
& > .ui.success.message,
& > .ui.warning.message,
& > .ui.error.message
{ display: block; }
`;

const roleIdMaps = {
  dev: {
    pup: '895974315047141408',
    handler: '895974386467766283',
    pandler: '895974407049195540',
    hehim: '895974431879495731',
    sheher: '895974455774425118',
    theythem: '895974481535856670',
    orange: '896023567265857616',
    green: '896023895344312350',
  },
  prod: {
    pup: '751063421449142302',
    handler: '751063466323869779',
    pandler: '751063589674287115',
    kitty: '785004427819089920',
    hehim: '779642608342663209',
    sheher: '779642591389679626',
    theythem: '779642622284922890',
    zizir: '779650753051623444',
    itits: '901764805818458125',
    red: '896205720838610954',
    orange: '896206763198648383',
    yellow: '896206815556173874',
    green: '896207011191087145',
    blue: '896207183438557194',
    purple: '896207335066853377',
    pink: '896207412787294279',
    black: '896207495368953886',
    white: '896950829356027924',
  },
};
const roleIdMap = roleIdMaps[ENV];

const getErrors = (path, errors) => {
  return errors.filter(e => _.isEqual(e.path, path));
}
const renderErrors = (path, errors) => {
  const pathErrors = getErrors(path, errors);
  if (pathErrors.length === 0) return null;
  if (pathErrors.length === 1) return pathErrors[0].message;
  return <Fragment>{ pathErrors.map((e, i) => <div key={ i }>{ e.message }</div>) }</Fragment>
}

const isValue = value => !(_.isNil(value) || _.isEmpty(value) || value === '');

const MemberSvcMessages = props => {
  const { svc } = props;
  return (
    <MessageWrapper>
      { svc.state.matches('member') && (
        <Message success>
          <Message.Header>Membership current</Message.Header>
          <p>Membership is current until December 31st { new Date().getFullYear() }.</p>
        </Message>
      ) }
      { svc.state.matches('lapsedMember') && (
        <Message warning>
          <Message.Header>Payment due</Message.Header>
          <p>Payment for { new Date().getFullYear() } is due. Membership isn't current until paid.</p>
          {/* '{/* stupid emacs bug */}
        </Message>
      ) }
      { svc.state.matches('resigned') && (
        <Message info>
          <Message.Header>Resigned</Message.Header>
          <p>Member has resigned from the association. This can happen automatically if membership lapses for 12 months.</p>
        </Message>
      ) }
      { svc.state.matches('expelled') && (
        <Message negative>
          <Message.Header>Expelled</Message.Header>
          <p>Member has been expelled from the association on disciplinary grounds.</p>
        </Message>
      ) }
    </MessageWrapper>
  )
}

const ApplyStep = props => {
  return (
    <Step disabled={ !props.done }>
      <Icon name="file alternate outline" />
      <Step.Content>
        <Step.Title>Application form</Step.Title>
        <Step.Description>Received by VicPAH secretary</Step.Description>
      </Step.Content>
    </Step>
  );
};
const PayStep = props => {
  return (
    <Step disabled={ !props.done }>
      <Icon name="dollar" />
      <Step.Content>
        <Step.Title>Payment received</Step.Title>
        <Step.Description>Membership dues paid</Step.Description>
      </Step.Content>
    </Step>
  );
};
const ApproveStep = props => {
  return (
    <Step disabled={ !props.done }>
      <Icon name="handshake outline" />
      <Step.Content>
        <Step.Title>Membership approved</Step.Title>
        <Step.Description>Approved by VicPAH committee</Step.Description>
      </Step.Content>
    </Step>
  );
};

const NewMemberProcess = connect(
  null,
  {
    clearSubmitApplicationState: a.clearSubmitApplicationState,
  }
)(props => {
  const { clearSubmitApplicationState, hasDirty, svc, values } = props;

  const [applyOpen, setApplyOpen] = useState(false);
  const [payOpen, setPayOpen] = useState(false);

  const handleApplyClick = useCallback(ev => {
    ev.preventDefault();
    setApplyOpen(true);
  }, [setApplyOpen]);
  const handleApplyClose = useCallback(() => {
    clearSubmitApplicationState();
    setApplyOpen(false);
  }, [clearSubmitApplicationState, setApplyOpen]);
  const handleApplyComplete = useCallback(() => {
    clearSubmitApplicationState();
    setApplyOpen(false);
    setPayOpen(true);
  }, [clearSubmitApplicationState, setApplyOpen, setPayOpen]);

  const handlePayClick = useCallback(ev => {
    ev.preventDefault();
    setPayOpen(true);
  }, [setPayOpen]);
  const handlePayClose = useCallback(() => {
    setPayOpen(false);
  }, [setPayOpen]);
  const handlePayComplete = useCallback(() => {
    setPayOpen(false);
  }, [setPayOpen]);

  const canApply = stateCan(svc.state, 'SUBMIT_APPLICATION');
  const canPay = stateCan(svc.state, 'PAY');
  const isApplyWait = svc.state.matches('applied');

  if (!(canApply || canPay || isApplyWait)) {
    return null;
  }

  const hasErrList = [];
  if (!isValue(values.displayName)) hasErrList.push('scene name');
  if (!isValue(values.givenName)) hasErrList.push('given name');
  if (!isValue(values.address)) hasErrList.push('postal address');
  const hasAll = _.isEmpty(hasErrList);

  const doneApply = !canApply && (isApplyWait || canPay);
  const donePay = doneApply && !canPay;
  const doneApprove = !canApply && !isApplyWait;

  const payFirst = donePay || !doneApprove;

  return (
    <Fragment>
      { canApply && (
        <Fragment>
          <Button disabled={ !props.emailVerified || !hasAll || hasDirty } onClick={ handleApplyClick }>Start application</Button>
          { !props.emailVerified && (
            <Label basic color='red' pointing='left'>
              Must complete email verification
            </Label>
          ) }
          { props.emailVerified && !hasAll && (
            <Label basic color='red' pointing='left'>
              Must complete { hasErrList.join(', ') }
            </Label>
          ) }
          { props.emailVerified && hasAll && hasDirty && (
            <Label basic color='red' pointing='left'>
              Must save profile
            </Label>
          ) }
        </Fragment>
      ) }
      { canPay && !canApply && (
        <Button onClick={ handlePayClick }>Pay membership dues</Button>
      ) }
      <Step.Group size="mini">
        <ApplyStep done={ doneApply } />
        { payFirst && <PayStep done={ donePay } /> }
        <ApproveStep done={ doneApprove } />
        { !payFirst && <PayStep done={ donePay } /> }
      </Step.Group>
      <MemberApplicationModal
        key={ `apply-${applyOpen}` }
        open={ applyOpen }
        onClose={ handleApplyClose }
        onComplete={ handleApplyComplete }
        memberId={ props.id }
        values={ values }
        />
      <MemberPaymentModal
        key={ `pay-${payOpen}` }
        open={ payOpen }
        onClose={ handlePayClose }
        onComplete={ handlePayComplete }
        memberId={ props.id }
        billingContact={ _.pick(values, ['givenName', 'familyName', 'email']) }
        />
    </Fragment>
  );
});

const EmailConfirm = connect(
  state => ({
    client: state.refreshClient,
  }),
  { getMember: a.getMember },
)(({ id, client, getMember }) => {
  const [value, setValue] = useState('');
  const [saving, setSaving] = useState(false);
  const handleChange = useCallback((ev, field) => {
    setValue(field.value);
  }, [setValue]);
  const handleSubmit = useCallback(() => {
    setSaving(true);
    const title = 'Email confirmation';
    client
      .post(`confirm_email/${id}`, { code: value })
      .then(() => {
        toast({
          type: 'success',
          description: 'Thanks for verifying your email',
          title,
        });
        getMember(id);
      })
      .catch(() => {
        setSaving(false);
        toast({
          type: 'error',
          description: 'Something went wrong. Check your confirmation code',
          title,
        });
      });
  }, [client, getMember, id, value]);
  const handleResend = useCallback(() => {
    setSaving(true);
    const title = 'Confirmation code';
    client
      .post(`confirm_email/${id}/resend`)
      .then(() => {
        setSaving(false);
        toast({
          type: 'success',
          description: 'Email has been resent',
          title,
        });
      })
      .catch(() => {
        setSaving(false);
        toast({
          type: 'error',
          description: 'Something went wrong',
          title,
        });
      });
  }, [id, client]);
  return (
    <Fragment>
      <Message
        content="You still need to confirm your email address"
        warning
        />
      <Form onSubmit={ handleSubmit }>
        <Form.Input
          label="Email Confirmation Code"
          name="code"
          value={ value }
          placeholder="You should have received this in your email"
          disabled={ saving }
          onChange={ handleChange }
          action={{ color: 'green', content: 'Submit' }}
          />
      </Form>
      <Button onClick={ handleResend } disabled={ saving } fluid>
        Resend verification email
      </Button>
      <Divider />
    </Fragment>
  );
});

const renderErrorsWithWrap = (path, errors) => {
  const errorMsg = renderErrors(path, errors);
  if (errorMsg) {
    return <Message error>{ errorMsg }</Message>
  }
}

const EditableMember = connect(
  (state, props) => ({
    ...s.patchMemberState(state || {}, { id: props.id }),
    isSecretaryScope: s.isSecretaryScope(state),
  }),
  { patchMember: a.patchMember },
)(props => {
  const { id, patchMember, emailVerified, isSecretaryScope } = props;
  const showFull = emailVerified || isSecretaryScope;

  const [values, setValues] = useState({
    ...(
      _.fromPairs(
        ([
          'displayName',
          'email',
          'givenName',
          'familyName',
          'mobileNumber',
          'address',
          'pronouns',
          // 'hideMemberList',
        ]).map(k => [k, _.get(props, k, '')])
      )
    ),
    password: '',
    passwordAgain: '',
  });
  const [dirty, setDirty] = useState({});
  const [errors, setErrors] = useState([]);
  useEffect(() => {
    if (props.errors)
      setErrors(props.errors);
  }, [props.errors]);
  const handleChange = useCallback((ev, field) => {
    const { name, value, checked, type } = field;
    setDirty({ ...dirty, [name]: true });
    setErrors(errors.filter(e => !_.isEqual(e.path, [name])));
    setValues({ ...values, [name]: (_.isNil(checked) || type === 'radio') ? value : checked });
  }, [dirty, errors, values]);
  useEffect(() => {
    if (!dirty.password || !dirty.passwordAgain) return;
    if (values.password !== values.passwordAgain) {
      setErrors([
        ...errors,
        { path: ['passwordAgain'], message: 'Must match password' },
      ]);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dirty.password, dirty.passwordAgain, values.password, values.passwordAgain]);
  const handleSubmit = useCallback(() => {
    setErrors([]);
    const fullPatch = {
      id,
      ..._.mapValues(values, (v, k) => {
        if (k === 'passwordAgain') return;
        if (k === 'password' && v === '') return;
        if (v === '') return null;
        return v;
      })
    }
    if (fullPatch.pronouns === '') fullPatch.pronouns = null;
    patchMember(
      showFull
        ? fullPatch
        : _.pick(fullPatch, ['id', 'password', 'email'])
    );
  }, [showFull, patchMember, id, values]);
  const globalErrors = getErrors([], errors);
  const saving = props.state === 'pending';
  useEffect(() => {
    if (props.state === 'success') {
      toast({ title: 'Saved profile', type: 'success' });
      setDirty({});
    }
  }, [props.state]);
  const hasDirty = !_.isEmpty(dirty);
  return (
    <Fragment>
      { !saving && !!globalErrors.length && (
        <Message
          header="Error saving profile"
          list={ globalErrors.map(e => e.message) }
          error
          />
      ) }
      { !emailVerified && <EmailConfirm id={ id } /> }
      <Form error onSubmit={ handleSubmit }>
        <Form.Input
          label="Scene Name"
          name="displayName"
          value={ values.displayName || '' }
          error={ renderErrors(['displayName'], errors) }
          placeholder="Pup name, online alias, etc"
          disabled={ saving || !showFull }
          onChange={ handleChange }
          />
        <Form.Input
          label="Email"
          name="email"
          value={ values.email || '' }
          error={ renderErrors(['email'], errors) }
          placeholder="pupper@example.com"
          disabled={ saving }
          onChange={ handleChange }
          />
        <Form.Group widths="equal">
          <Form.Input
            label="Change password"
            name="password"
            value={ values.password || '' }
            error={ renderErrors(['password'], errors) }
            type="password"
            disabled={ saving }
            onChange={ handleChange }
            />
          <Form.Input
            label="Password again"
            name="passwordAgain"
            value={ values.passwordAgain || '' }
            error={ renderErrors(['passwordAgain'], errors) }
            type="password"
            disabled={ saving }
            onChange={ handleChange }
            />
        </Form.Group>
        <Form.Group inline>
          <span className={`field ${saving || !showFull ? 'disabled' : ''}`}><label>Pronouns</label></span>
          <Form.Field
            control={ Radio }
            label="He/Him"
            value="he_him"
            name="pronouns"
            checked={ values.pronouns === 'he_him' }
            disabled={ saving || !showFull }
            onChange={ handleChange }
            />
          <Form.Field
            control={ Radio }
            label="She/Her"
            value="she_her"
            name="pronouns"
            checked={ values.pronouns === 'she_her' }
            disabled={ saving || !showFull }
            onChange={ handleChange }
            />
          <Form.Field
            control={ Radio }
            label="They/Them"
            value="they_them"
            name="pronouns"
            checked={ values.pronouns === 'they_them' }
            disabled={ saving || !showFull }
            onChange={ handleChange }
            />
          <Form.Field
            control={ Radio }
            label="Zi/Zir"
            value="zi_zir"
            name="pronouns"
            checked={ values.pronouns === 'zi_zir' }
            disabled={ saving || !showFull }
            onChange={ handleChange }
            />
          <Form.Field
            control={ Radio }
            label="It/Its"
            value="it_its"
            name="pronouns"
            checked={ values.pronouns === 'it_its' }
            disabled={ saving || !showFull }
            onChange={ handleChange }
            />
          <Form.Field
            control={ Radio }
            label="Unspecified"
            value={ '' }
            name="pronouns"
            checked={ !values.pronouns }
            disabled={ saving || !showFull}
            onChange={ handleChange }
            />
        </Form.Group>
        { renderErrorsWithWrap(['pronouns'], errors) }
        <p>Don't see your pronouns? Contact support@vicpah.org.au</p>
        {/* '{/* stupid emacs bug */}
        <Form.Group widths="equal">
          <Form.Input
            label="Given Name"
            name="givenName"
            value={ values.givenName || '' }
            error={ renderErrors(['givenName'], errors) }
            disabled={ saving || !showFull }
            onChange={ handleChange }
            />
          <Form.Input
            label="Family Name"
            name="familyName"
            value={ values.familyName || '' }
            error={ renderErrors(['familyName'], errors) }
            disabled={ saving || !showFull }
            onChange={ handleChange }
            />
        </Form.Group>
        <Form.Input
          label="Mobile Number"
          name="mobileNumber"
          value={ values.mobileNumber || '' }
          error={ renderErrors(['mobileNumber'], errors) }
          disabled={ saving || !showFull }
          onChange={ handleChange }
          />
        <Form.TextArea
          label="Postal Address"
          name="address"
          value={ values.address || '' }
          error={ renderErrors(['address'], errors) }
          disabled={ saving || !showFull }
          onChange={ handleChange }
          />
        { /* <Form.Input
          label="Date of Birth"
          defaultValue={ props.postalAddress }
              /> */ }
        {/* <Form.Checkbox
          label="Hide in Members List"
          name="hideMemberList"
          checked={ values.hideMemberList }
          error={ renderErrors(['hideMemberList'], errors) }
          onChange={ handleChange }
          disabled={ saving }
          toggle
          /> */ }
        <Form.Field>
          <label>Membership status</label>
          <MemberSvcMessages { ..._.pick(props, ['svc', 'svcState']) } />
          <NewMemberProcess
            values={ values }
            hasDirty={ hasDirty }
            { ..._.pick(props, ['id', 'svc', 'svcState', 'emailVerified']) }
            />
        </Form.Field>
        <Form.Field>
          <label>Join Date</label>
          { props.joinDate.format('YYYY, MMMM Do') }
        </Form.Field>
        <BottomFixed>
          <Container className="content" style={{ display: 'flex' }} text>
            <div style={{ flexGrow: 1 }} />
            <div style={{ flexShrink: 1 }}>
              <Button loading={ saving } disabled={ saving || !hasDirty } positive>Save</Button>
            </div>
          </Container>
        </BottomFixed>
      </Form>
    </Fragment>
  );
});
const ErrorMember = props => {
  return (
    <Message negative>
      <Message.Header>Unauthorized</Message.Header>
      <p>Either you're not logged in, or you don't have permission to access this member</p>
    </Message>
  );
};

const discordColorItem = name => ({
  key: name,
  text: name,
  value: roleIdMap[name.toLowerCase()],
  label: {
    color: name === 'White' ? null : name.toLowerCase(),
    empty: true,
  },
});
const discordPetPlayRoleItem = (name, icons) => ({
  key: name,
  text: name,
  value: roleIdMap[name.toLowerCase()],
  image: icons[name.toLowerCase() + 's'],
});

const DiscordProfile = connect(
  (state, props) => ({
    patchMemberState: s.patchMemberState(state || {}, { id: props.id }) || {},
    oauthState: state.oauthState,
    isAdmin: s.isAdminScope(state),
    refreshClient: state.refreshClient,
  }),
  {
    patchMember: a.patchMember,
  },
)(props => {
  const {
    discordId, id, oauthState, isAdmin, refreshClient, isCurrentUser,
    patchMember, patchMemberState: { state: patchMemberState },
  } = props;

  if (!isCurrentUser) {
    return <Message content="Can only manage Discord for your own user" error />
  }

  const [linkLoading, setLinkLoading] = useState(false);
  const [delinkLoading, setDelinkLoading] = useState(false);
  const handleDiscordLogin = useCallback(() => {
    setLinkLoading(true);
    document.location = DISCORD_OAUTH_URL;
  }, [setLinkLoading]);
  const handleDiscordBotLogin = useCallback(() => {
    setLinkLoading(true);
    document.location = DISCORD_ADMIN_OAUTH_URL;
  }, [setLinkLoading]);
  const handleDelink = useCallback(() => {
    setDelinkLoading(true);
    patchMember({ id, discordId: null });
  }, [id, patchMember, setDelinkLoading]);
  useEffect(() => {
    if (patchMemberState !== 'pending')
      setDelinkLoading(false);
  }, [patchMemberState, setDelinkLoading]);
  useEffect(() => {
    if (oauthState === 'pending')
      setLinkLoading(true);
  }, [oauthState, setLinkLoading]);

  const [saving, setSaving] = useState(false);
  const [hasDirty, setHasDirty] = useState(false);
  const [loadingServer, setLoadingServer] = useState(false);
  const [loadingMember, setLoadingMember] = useState(false);
  const [serverData, setServerData] = useState(null);
  const [memberRoles, setMemberRoles] = useState(null);

  const handleMemberRes = useCallback(res => {
    setMemberRoles(res.data.roles.map(role => role.id))
    setLoadingMember(false);
  }, [setMemberRoles, setLoadingMember]);

  useEffect(() => {
    setLoadingMember(true);
    setLoadingServer(true);
    refreshClient.get('/discord_member').then(handleMemberRes);
    refreshClient.get('/discord_info').then(res => {
      setServerData(res.data)
      setLoadingServer(false);
    });
  }, [discordId, refreshClient, setLoadingServer, setLoadingMember, handleMemberRes, setServerData]);

  const petPlayRoles = (memberRoles || []).filter(id => (
    [
      roleIdMap.pup,
      roleIdMap.handler,
      roleIdMap.pandler,
      roleIdMap.kitty,
    ].indexOf(id) >= 0
  ));
  const pronounRoles = (memberRoles || []).filter(id => (
    [
      roleIdMap.hehim,
      roleIdMap.sheher,
      roleIdMap.theythem,
      roleIdMap.zizir,
      roleIdMap.itits,
    ].indexOf(id) >= 0
  ));
  const colorRoles = (memberRoles || []).filter(id => (
    [
      roleIdMap.red,
      roleIdMap.orange,
      roleIdMap.yellow,
      roleIdMap.green,
      roleIdMap.blue,
      roleIdMap.purple,
      roleIdMap.pink,
      roleIdMap.black,
      roleIdMap.white,
    ].indexOf(id) >= 0
  ));

  const handleChange = useCallback((ev, data) => {
    setHasDirty(true);
    const arrayValue = _.isArray(data.value)
      ? data.value
      : (data.value ? [data.value] : []);
    switch(data.name) {
      case 'color':
        setMemberRoles([...petPlayRoles, ...pronounRoles, ...arrayValue]);
        break;
      case 'petPlayRole':
        setMemberRoles([...pronounRoles, ...colorRoles, ...arrayValue]);
        break;
      case 'pronouns':
        setMemberRoles([...petPlayRoles, ...colorRoles, ...arrayValue]);
        break;
      default:
        console.error('Unknown field:', data.name);
    }
  }, [colorRoles, petPlayRoles, pronounRoles, setMemberRoles]);
  const handleSubmit = useCallback(() => {
    setSaving(true);
    refreshClient
      .post('/discord_member', { roles: memberRoles })
      .then(res => {
        handleMemberRes(res);
        setSaving(false);
      });
  }, [setSaving, refreshClient, memberRoles, handleMemberRes]);

  const profileLoading = loadingServer || loadingMember || saving;

  console.log('server data', serverData);
  const roleIcons = _.fromPairs(((serverData||{}).roles||[]).map(role => [
    role.name.toLowerCase(),
    role.icon ?
      `https://cdn.discordapp.com/role-icons/${role.id}/${role.icon}.png`
      : null,
  ]));
  // const roleIcons = {
  //   pup: 'https://cdn.discordapp.com/role-icons/751063421449142302/625720ebb0377b8f71dd340de9285d5c.png',
  //   handler: 'https://cdn.discordapp.com/role-icons/751063466323869779/d62a7a0351dad3ea3bb9bb73821f08d7.png',
  //   pandler: 'https://cdn.discordapp.com/role-icons/751063589674287115/0548be9a102af6e9b80e03760f78d928.png',
  //   kitty: 'https://cdn.discordapp.com/role-icons/785004427819089920/018f99e1497dadd742e8d6621c170657.png',
  // };

  return (
    <Fragment>
      { discordId
        ? (
          <Fragment>
            <Message
              icon="discord"
              content={
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <div style={{ flexGrow: 1 }}>
                    Your profile is linked to a Discord user!
                  </div>
                  <div>
                    <Button
                      onClick={ handleDelink }
                      loading={ delinkLoading }
                      negative
                      basic
                    >Delink</Button>
                  </div>
                </div>
              }
              positive
            />
            <Form error onSubmit={ handleSubmit }>
              <Loader active={ profileLoading } />
              <Form.Dropdown
                label="Color"
                name="color"
                options={[
                  discordColorItem('Red'),
                  discordColorItem('Orange'),
                  discordColorItem('Yellow'),
                  discordColorItem('Green'),
                  discordColorItem('Blue'),
                  discordColorItem('Purple'),
                  discordColorItem('Pink'),
                  discordColorItem('Black'),
                  discordColorItem('White'),
                ]}
                value={ colorRoles[0] }
                onChange={ handleChange }
                disabled={ profileLoading }
                fluid
                clearable
                selection
                upward
                labeled
                />
              <Form.Dropdown
                label="Pet play role"
                name="petPlayRole"
                options={[
                  discordPetPlayRoleItem('Pup', roleIcons),
                  discordPetPlayRoleItem('Handler', roleIcons),
                  discordPetPlayRoleItem('Pandler', roleIcons),
                  discordPetPlayRoleItem('Kitty', roleIcons),
                ]}
                value={ petPlayRoles[0] }
                onChange={ handleChange }
                disabled={ profileLoading }
                fluid
                clearable
                selection
                upward
                labeled
                />
              <Form.Dropdown
                label="Pronouns"
                name="pronouns"
                options={[
                  {key: 'sheher', text: 'She/Her', value: roleIdMap.sheher},
                  {key: 'hehim', text: 'He/Him', value: roleIdMap.hehim},
                  {key: 'theythem', text: 'They/Them', value: roleIdMap.theythem},
                  {key: 'zizir', text: 'Zi/Zir', value: roleIdMap.zizir},
                  {key: 'itits', text: 'It/Its', value: roleIdMap.itits},
                ]}
                value={ pronounRoles }
                onChange={ handleChange }
                disabled={ profileLoading }
                fluid
                multiple
                selection
                upward
                />
              <BottomFixed>
                <Container className="content" style={{ display: 'flex' }} text>
                  <div style={{ flexGrow: 1 }} />
                  <div style={{ flexShrink: 1 }}>
                    <Button loading={ saving } disabled={ saving || !hasDirty } positive>Save</Button>
                  </div>
                </Container>
              </BottomFixed>
            </Form>
          </Fragment>
        )
        : (
          <Button
            onClick={ handleDiscordLogin }
            color="violet"
            loading={ linkLoading }
            fluid
          >
            <Icon name="discord" />
            Link with Discord
          </Button>
        )
      }
      { isAdmin && <Button
        onClick={ handleDiscordBotLogin }
        color="red"
        loading={ linkLoading }
        fluid
      >
        <Icon name="discord" />
        ADMIN BOT Link with Discord
      </Button> }
    </Fragment>
  )
});


export const MemberPage = connect(
  (state, props) => ({
    isCurrentUser: props.match.params.slug === (s.getLoggedIn(state) || {}).id,
    member: s.getMember(state || {}, { id: props.match.params.slug }),
    ...s.getMemberState(state || {}, { id: props.match.params.slug }),
  }),
  {
    getMember: a.getMember
  },
)(props => {
  const { history, location, match, member, state, isCurrentUser } = props;
  const { params: { slug } } = match;

  if (!location.pathname.endsWith('/')) {
    history.replace(`${location.pathname}/`);
    return null;
  }

  useEffect(() => {
    props.getMember(slug);
  }, [slug]);  // eslint-disable-line react-hooks/exhaustive-deps

  const [svcState, setSvcState] = useState(null);
  const stateLog = (member || {}).stateLog;
  const svc = useMemo(() => {
    const s = stateLogToMemberSvc(stateLog, (member || {}).id);
    setSvcState(s.state.value);
    return s;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stateLog]);
  useEffect(() => {
    const handler = state => {
      setSvcState(state);
    }
    svc.onTransition(handler);
    window.memberSvc = svc;
    return () => {
      svc.off(handler);
      delete window.memberSvc;
    }
  }, [svc]);

  const [currTabIdx, setCurrTabIdx] = useState(0);
  const setProfileTab = useCallback(() => {
    setCurrTabIdx(0);
  }, [setCurrTabIdx]);
  const setDiscordTab = useCallback(() => {
    setCurrTabIdx(1);
  }, [setCurrTabIdx]);
  const handleTabChange = useCallback((ev, data) => {
    const key = data.panes[data.activeIndex].menuItem.key;

    if (key === 'profile') {
      history.push(`${match.url}`);
    } else {
      history.push(`${match.url}${key}/`);
    }
  }, [history, match]);
  const renderProfileTab = useCallback(() => (
    <EditableMember { ...member } svc={ svc } svcState={ svcState } />
  ), [member, svc, svcState]);
  const renderDiscordTab = useCallback(() => (
    <DiscordProfile isCurrentUser={ isCurrentUser } { ...member } />
  ), [isCurrentUser, member]);

  if (state === 'pending') {
    return <Loader active inline='centered' />;
  }

  return (
    <Container className="content" text>
      <Switch>
        <Route path={ `${match.path}discord/` } render={ setDiscordTab } />
        <Route render={ setProfileTab } />
      </Switch>
      { !isCurrentUser && <Message
        content="Viewing another member's profile"
        warning
      /> }
      { member
        ? (
          <Tab
            menu={{ secondary: true, pointing: true }}
            onTabChange={ handleTabChange }
            activeIndex={ currTabIdx }
            panes={[
              {
                menuItem: { key: 'profile', content: 'Profile', icon: 'user' },
                render: renderProfileTab,
              },
              {
                menuItem: { key: 'discord', content: 'Discord', icon: 'discord' },
                render: renderDiscordTab,
              },
            ]}
          />
        )
        : <ErrorMember />
      }
    </Container>
  );
});
