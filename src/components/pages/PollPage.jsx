import { useLazyQuery, useMutation, useQuery, gql } from '@apollo/client';
import _ from 'lodash';
import React, { PureComponent, useEffect, useState } from 'react';
import { Route, Switch } from 'react-router';
import { toast } from 'react-semantic-toasts';
import { Button, Container, Icon, /*Input, */Loader, Message, Search } from 'semantic-ui-react';
import styled, { withTheme } from 'styled-components/macro';

const POLL_BY_ID_Q = gql`
  query PollById($id: ID!) {
    pollById(id: $id) {
      success
      message
      poll {
        title
        description
        options {
          id
          text
          disabled
          meta
          votesCount
        }
        selfVote {
          id
          optionIds
        }
      }
    }
  }
`;
const CREATE_OPTION_M = gql`
  mutation CreateOption($option: CreateOption) {
    createOption(option: $option) {
      success
      message
      option {
        id
      }
    }
  }
`;
const CREATE_VOTE_M = gql`
  mutation CreateVote($vote: CreateVote) {
    createVote(vote: $vote) {
      success
      message
    }
  }
`;
const OPTION_QUERY_Q = gql`
  query OptionQuery($group: String!, $query: String!) {
    optionQuery(group: $group, query: $query) {
      id
      text
      meta
    }
  }
`;

export const PollPageRoute = ({ match }) => (
  <Switch>
    <Route path={ `${match.path}:pollId` } exact component={ PollPage } />
    <Route path="/">
      <h1>No such poll.</h1>
    </Route>
  </Switch>
);

const PollPage = props => {
  const { match: { params: { pollId } } } = props;
  const q = useQuery(
    POLL_BY_ID_Q,
    { variables: { id: pollId } },
  );
  const [createOption, mo] = useMutation(
    CREATE_OPTION_M,
  );
  const [createVote, mv] = useMutation(
    CREATE_VOTE_M,
  );
  const [wantVotes, setWantVotes] = useState(null);

  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    if (!mo.called || mo.loading) return
    q.refetch();
  }, [mo.loading]);
  useEffect(() => {
    if (!mv.called || mv.loading) return
    q.refetch();
  }, [mv.loading]);

  useEffect(() => {
    if (!mo.called) return;
    if (mo.loading) return;
    const success = _.get(mo, 'data.createOption.success', null);
    if (success === true && !mo.error) return;
    toast({
      title: 'Error adding option',
      description: _.get(mo, 'data.createOption.message', null),
      type: 'error',
    });
  }, [
    _.get(mo, 'data.createOption.success'),
    mo.error,
  ]);
  useEffect(() => {
    if (!mv.called) return;
    if (mv.loading) return;
    const success = _.get(mv, 'data.createVote.success', null);
    if (success === true && !mv.error) return;
    toast({
      title: 'Error voting',
      description: _.get(mv, 'data.createOption.message', null),
      type: 'error',
    });
  }, [
    _.get(mv, 'data.createVote.success'),
    mv.error,
  ]);

  useEffect(() => {
    if (!wantVotes) return;
    if (mv.loading) return;
    createVote({ variables: { vote: { pollId, optionIds: wantVotes } } });
  }, [wantVotes, mv.loading]);
  useEffect(() => {
    if (!mv.called) return;
    if (mv.loading) return;
    setWantVotes(null);
    q.refetch();
  }, [mv.loading]);
  /* eslint-enable react-hooks/exhaustive-deps */

  return (
    <PollPageDisplay
      loading={ q.loading }
      error={ q.error }
      data={ q.data }
      refetch={ q.refetch }
      createOption={ createOption }
      saving={ mo.loading }
      saveError={ mo.error }
      createVote={ createVote }
      voting={ mv.loading }
      voteError={ mv.error }
      wantVotes={ wantVotes }
      setWantVotes={ setWantVotes }
      { ...props }
    />
  );
}

const VoteButton = props => {
  const {
    optionId,
    selfVoteIds,
    wantVotes,
    setWantVotes,
    style,
  } = props;

  const isCurrent = (selfVoteIds || []).indexOf(optionId) >= 0;
  const isWanted = (wantVotes || []).indexOf(optionId) >= 0;
  const loading = wantVotes && isCurrent !== isWanted;
  const color = isCurrent ? 'green' : null;

  const handleClick = () => {
    const all = [
      ...(selfVoteIds || []),
      ...(wantVotes || []),
    ];
    if (isCurrent || isWanted) {
      setWantVotes(all.filter(id => id !== optionId));
    } else {
      setWantVotes([...all, optionId]);
    }
  };

  return (
    <Button
      color={ color }
      loading={ loading }
      onClick={ handleClick }
      style={ style }
    >
      <Icon name="check" />
    </Button>
  );
}

// const AddOptionInput = props => {
//   const { createOption, pollId, saving } = props;
//   const [value, setValue] = useState('');
//   const handleChange = (ev, data) => {
//     setValue(data.value);
//   }
//   const handleAdd = () => {
//     createOption({ variables: { option: {
//       pollId,
//       text: value,
//     } } });
//   }
//   useEffect(() => {
//     if (!saving)
//       setValue('');
//   }, [saving]);

//   return (
//     <Input
//       placeholder="Your idea..."
//       value={ value }
//       onChange={ handleChange }
//       action={{
//         content: "Add new option",
//         color: 'green',
//         icon: 'checkmark',
//         labelPosition: 'right',
//         loading: saving,
//         onClick: handleAdd,
//       }}
//     />
//   );
// }
const renderResult = props => {
  return <img src={ props.image } alt={ props.title } style={{ width: '100%' }} />;
}
const justWatchImage = item => (
  'https://images.justwatch.com' +
    JSON.parse(item.meta).justWatchPoster.replace('{profile}', 's718')
);
const AddOptionInput = props => {
  const { createOption, pollId } = props;
  const [queryRaw, { loading, data }] = useLazyQuery(
    OPTION_QUERY_Q,
  );
  const query = _.debounce(queryRaw, 500, { trailing: true });
  const handleSearchChange = (ev, data) => {
    query({ variables: { group: 'netflix', query: data.value }});
  }
  const results = ((data || {}).optionQuery || [])
        .map(item => ({
          title: item.text,
          image: justWatchImage(item),
          meta: item.meta,
        }));
  const handleResultSelect = (ev, data) => {
    createOption({ variables: { option: {
      pollId,
      text: data.result.title,
      meta: data.result.meta,
    } } });
  }
  return (
    <Search
      placeholder="Add a new option..."
      loading={ loading }
      results={ results }
      resultRenderer={ renderResult }
      onResultSelect={ handleResultSelect }
      onSearchChange={ handleSearchChange }
    />
  );
}

const RowsWrapper = styled.div`
display: grid;
grid-template-columns: 60px auto;
row-gap: 10px;
column-gap: 10px;
`;
const MovieOptionsWrapper = styled.div`
display: grid;
grid-template-columns: 25% 25% 25% 25%;
row-gap: 30px;
column-gap: 10px;
`;
const MovieOptionWrapper = styled.div`
position: relative;
`;

const pollType = 'netflix';

const PollVotesBar = props => (
  props.votesCount === 0
    ? <em style={{ opacity: 0.5 }}>No votes yet</em>
    : (
      <div style={{
             backgroundColor: props.color,
             borderRadius: '5px',
             color: 'white',
             textAlign: 'right',
             paddingRight: '10px',
             width: `${props.percent}%`,
           }}
           >
        { props.votesCount }
      </div>
    )
);
const OptionRow = props => (
  <React.Fragment key={ props.option.id }>
    <VoteButton
      optionId={ props.option.id }
      selfVoteIds={ _.get(props.data, 'pollById.poll.selfVote.optionIds', []) }
      wantVotes={ props.wantVotes }
      setWantVotes={ props.setWantVotes }
    />
    <div>
      <div>
        { props.option.text }
      </div>
      <PollVotesBar
        votesCount={ props.option.votesCount }
        percent={ props.percent }
        color={ props.theme.primary }
        />
    </div>
  </React.Fragment>
);
const MovieOption = props => (
  <MovieOptionWrapper>
    { props.option.meta
      ? <img
            src={ justWatchImage(props.option) }
            alt={ props.option.text }
            width='100%'
            />
      : props.option.text
    }
    <PollVotesBar
      votesCount={ props.option.votesCount }
      percent={ props.percent }
      color={ props.theme.primary }
      />
    <VoteButton
      optionId={ props.option.id }
      selfVoteIds={ _.get(props.data, 'pollById.poll.selfVote.optionIds', []) }
      wantVotes={ props.wantVotes }
      setWantVotes={ props.setWantVotes }
      style={{ position: 'absolute', top: '5px', right: '5px', opacity: 0.7 }}
    />
  </MovieOptionWrapper>
);

@withTheme
class PollPageDisplay extends PureComponent {
  render() {
    const {
      data,
      loading,
      saving,
      createOption,
      match: { params: { pollId } },
    } = this.props;

    const options = _.get(data, 'pollById.poll.options', [])
          .filter(option => !option.disabled);
    const maxVotes = Math.max(...(options.map(option => option.votesCount)));

    const OptionsWrapper = pollType === 'netflix'
          ? MovieOptionsWrapper
          : RowsWrapper;
    const Option = pollType === 'netflix'
          ? MovieOption
          : OptionRow;

    return (
      <Container className="content" text>
        { loading && <Loader active inline='centered' size="massive">Loading poll</Loader> }
        { data && !data.pollById.success && (
          <Message error>{ data.pollById.message }</Message>
        ) }
        { data && data.pollById.success && (
          <>
            <h1>{ data.pollById.poll.title }</h1>
            <p>{ data.pollById.poll.description }</p>
            { options.length > 0
              ? (
                <OptionsWrapper>
                  { options
                    .map((option, idx) => {
                      const percent = maxVotes === 0
                            ? 0
                            : option.votesCount / maxVotes * 100;
                      return <Option { ...this.props } option={ option } percent={ percent } />;
                    })
                  }
                </OptionsWrapper>
              )
              : <Message warning>No votes have been cast yet</Message>
            }
            <div style={{ marginTop: '10px'}}>
              <AddOptionInput createOption={ createOption } pollId={ pollId } saving={ saving } />
            </div>
          </>
        ) }
      </Container>
    );
  }
}
export { PollPage };
