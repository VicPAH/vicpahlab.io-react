import React, { PureComponent } from 'react';

import { Event, EventsList } from '../EventsList';
import { Calendar } from '../../utils/ical';


export class IcalPage extends PureComponent {
  state = {
    calendar: null,
  }
  componentDidMount() {
    this.updateIcal()
  }
  componentDidUpdate(prevProps) {
    if (this.props.response !== prevProps.response) {
      this.updateIcal()
    }
  }
  updateIcal = () => {
    const { response: { data } } = this.props;
    this.setState({ calendar: Calendar.fromString(data) })
  }
  render() {
    const { calendar } = this.state;
    if (!calendar) {
      return null;
    }
    return (
      <EventsList>
        { calendar.getEvents().map(ev => <Event key={ ev.getUid() } event={ ev } />) }
      </EventsList>
    );
  }
}
