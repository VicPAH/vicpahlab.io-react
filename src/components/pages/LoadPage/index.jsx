import axios from 'axios';
import _ from 'lodash';
import React, { Fragment, PureComponent } from 'react';
import { Container, Dimmer, Loader, Message } from 'semantic-ui-react';

import { ResponseContent } from './ResponseContent';
import { ResponseError } from './ResponseError';
import { getDisplayName } from '../../../utils/react';


export const createLoadPageRoute = LoadPageComponent => class LoadPageRoute extends PureComponent {
  static displayName = `LoadPageRoute(${getDisplayName(LoadPageComponent)})`
  state = {
    url: null,
  }
  componentDidMount() {
    this.refreshUrl(this.props.match.url);
  }
  componentDidUpdate(prevProps) {
    if (this.props.match.url !== prevProps.match.url) {
      this.refreshUrl(this.props.match.url);
    }
  }
  refreshUrl = url => {
    const dotIdx = url.lastIndexOf('.');
    const slashIdx = url.lastIndexOf('/');
    const isFile = dotIdx > slashIdx;
    const isDir = url.endsWith('/');

    if (!(isFile || isDir)) {
      this.props.history.replace(`${url}/`);
      return;
    }

    this.setState({ url: `/content${url}index.md` });
  }
  render() {
    const { url } = this.state;
    if (!url) {
      return null;
    }
    return <LoadPageComponent url={ url } { ..._.omit(this.props, ['match']) } />
  }
}

export const createLoadPage = (
  ResponseContentComponent = ResponseContent,
  ResponseErrorComponent = ResponseError,
) => class LoadPage extends PureComponent {
  static displayName = `LoadPage(${getDisplayName(ResponseContentComponent)},${getDisplayName(ResponseErrorComponent)})`
  static defaultProps = {
    showNotFound: true,
  }
  state = {
    hasError: false,
    response: null,
    err: null,
    loading: false,
  }
  static getDerivedStateFromError() {
    return { hasError: true };
  }
  componentDidMount() {
    this.refreshContent(this.props.url);
  }
  componentDidUpdate(prevProps) {
    if (this.props.url !== prevProps.url) {
      this.refreshContent(this.props.url);
    }
  }
  refreshContent = async url => {
    if (this.state.cancel) {
      this.state.cancel.cancel();
    }

    const cancel = axios.CancelToken.source();
    this.setState({ loading: true, response: null, cancel })

    let response
    try {
      response = await axios.get(
        url,
        {
          responseType: 'text',
          validateStatus: () => true,
          cancelToken: cancel.token,
        },
      );
    } catch(err) {
      if (axios.isCancel(err)) {
        return;
      }
      // TODO show errors
      this.setState({ loading: false, cancel: null, err });
      return;
    }
    this.setState({ loading: false, cancel: null, response });
  }
  renderResponse = () => {
    const { contentType } = this.props;
    const { response } = this.state
    if (!response) {
      return null
    }
    const props = _.omit(this.props, ['loading', 'noContainer', 'no404', 'contentType'])
    if (response.status === 404 && this.props.no404) return null;
    if (
      contentType &&
        response.headers['content-type'] &&
        response.headers['content-type'].split(';')[0].trim() !== contentType
    )
      return null;
    return response.status === 200
      ? <ResponseContentComponent response={ response } { ...props } />
      : <ResponseErrorComponent response={ response }{ ...props } />
  }
  render() {
    if (this.state.hasError) {
      return <Message error>Problem loading page</Message>
    }
    const { noContainer } = this.props
    const { loading } = this.state
    if (noContainer) {
      return (
        <Fragment>
          { this.renderResponse() }
          <Dimmer active={ loading } inverted><Loader /></Dimmer>
        </Fragment>
      )
    }
    return (
      <Dimmer.Dimmable as={ Container } dimmed={ loading }>
        <Container className="content" text>
          { this.renderResponse() }
        </Container>
        <Dimmer active={ loading } inverted><Loader /></Dimmer>
      </Dimmer.Dimmable>
    )
  }
}

export const LoadPage = createLoadPage()
export const LoadPageRoute = createLoadPageRoute(LoadPage)
