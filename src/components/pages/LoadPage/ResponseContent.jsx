import React from 'react';

import { IcalPage } from '../IcalPage';
import { MarkdownPage } from '../MarkdownPage';


export const ResponseContent = props => {
  let Component;
  const { response: { headers: { 'content-type': contentType } } } = props;
  if (contentType.toLowerCase().startsWith('text/markdown')) {
    Component = MarkdownPage;
  } else if (contentType.toLowerCase().startsWith('text/calendar')) {
    Component = IcalPage;
  }

  return <Component { ...props } />
}
