import React, { PureComponent } from 'react';


class NotFoundPage extends PureComponent {
  render() {
    return 'Page not found';
  }
}
class DefaultErrorPage extends PureComponent {
  render() {
    return this.props.response.statusText;
  }
}


export const ResponseError = props => {
  let Component;
  const {
    response: { status },
    showNotFound,
  } = props;
  if (status === 404) {
    Component = showNotFound ? NotFoundPage : null;
  } else {
    Component = DefaultErrorPage;
  }

  if (!Component) {
    return null;
  }

  return <Component { ...props } />
}
