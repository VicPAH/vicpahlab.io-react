import axios from 'axios';
import BLAKE2s from 'blake2s-js';
import _ from 'lodash';
import React, { Fragment, PureComponent } from 'react';
import reactEmojify from 'react-emoji';
import ReCAPTCHA from "react-google-recaptcha";
import { Route, Switch, withRouter } from 'react-router';
import { Button, Checkbox, Container, Dimmer, Form, Header, Icon, Image, Input, Label, Loader, Message, Modal, Portal, Progress, Rail, Segment } from 'semantic-ui-react';
import * as Sentry from '@sentry/browser';
import styled from 'styled-components/macro';
import color from 'tinycolor2';

import { MaybeLink } from '../../MaybeLink';

import eagleLogo from '../../../img/eagle_leather_photo_comp_logo.png';


const FormHelp = styled.div`
color: rgba(0,0,0,0.5);
font-size: ${props => props.size || '0.75em'};
`
const FormError = styled(FormHelp)`
color: #9f3a38;
`


/* global PHOTO_COMP_2020_API_URL */
/* global PHOTO_COMP_2020_STATIC_URL */
/* global RECAPTCHA_KEY */
const URL_PREFIX = PHOTO_COMP_2020_API_URL;
const STATIC_DOMAIN = PHOTO_COMP_2020_STATIC_URL;
const LS_KEY = 'au.org.vicpah.photocomp';


const getStore = () => JSON.parse(localStorage[LS_KEY] || '{}');
const setStore = data => localStorage[LS_KEY] = JSON.stringify(data);
const buildAddArrayStore = key => hash => {
  const data = getStore();
  const arr = data[key] || [];
  arr.push(hash);
  setStore({
    ...data,
    [key]: _.uniq(arr),
  });
};
const buildCheckArrayStore = key => hash => (getStore()[key] || []).indexOf(hash) !== -1;
const addOwned = buildAddArrayStore('ownedHashes');
const checkOwned = buildCheckArrayStore('ownedHashes');
const addVote = buildAddArrayStore('votesCast');
const checkVote = () => false; //buildCheckArrayStore('votesCast');
const getToken = () => {
  const token = getStore().token;
  if (_.isPlainObject(token) && token.body) {
    return JSON.parse(token.body).token;
  }
  return token;
};
const setToken = token => setStore({ ...getStore(), token });

const getApi = () => {
  const token = getToken();
  return axios.create({
    baseURL: URL_PREFIX,
    headers: {
      ...(token ? { Authorization: `Bearer ${token}` } : {})
    },
    validateStatus: status => {
      if (status >= 500) {
        Sentry.captureException(new Error(`HTTP backend error ${status}`));
      }
      return status >= 200 && status < 400;
    },
  })
};


const getPhotoUrl = (hash, size) => `https://${STATIC_DOMAIN}/${hash}x${size}.png`;
const getVoteUrlPath = hash => `/photo-comp-2020/vote-for/${hash}`
const getVoteUrl = hash => `${document.location.origin}${getVoteUrlPath(hash)}`;

const emojify = (text, opts={}) => reactEmojify.emojify(text, { emojiType: 'emojione', ...opts })


class EnsureToken extends PureComponent {
  state = {
    step: 'user',
  }
  handleChange = async token => {
    this.setState({ step: 'token' });
    try {
      const resp = await getApi().post(`/token`, { recaptcha: token });
      setToken(resp.data.token);
      this.setState({ step: 'user' });
      if (this.props.onGotToken) {
        this.props.onGotToken();
      }
    } catch(err) {
      console.error('Error getting token:', err);
      this.setState({ step: 'error' });
    }
  }
  render() {
    const { step } = this.state;
    if (getToken()) {
      return null;
    }
    return (
      <Modal size="small" open={ true } basic>
        { step === 'user' && (
          <Fragment>
            <Header content={ "Just need to check that you're not a robot *beep boop*" } />
            <Modal.Content>
              <ReCAPTCHA
                sitekey={ RECAPTCHA_KEY }
                onChange={ this.handleChange }
                theme="dark"
                />
            </Modal.Content>
          </Fragment>
        ) }
      { step === 'token' && (
        <Modal.Content>
          <Loader active inline='centered'>*internet noises*</Loader>
        </Modal.Content>
      ) }
      { step === 'error' && (
        <Fragment>
          <Header content={ "Sorry! Something's gone wrong" } />
          <Modal.Content>
            <p>Please try again later, and if that doesn't work, send an email to biru@vicpah.org.au</p>{/*'*/}
          </Modal.Content>
        </Fragment>
      ) }
      </Modal>
    );
  }
}


const ShareButtonColored = styled(Button)`
background-color: ${props => props.color} !important;
color: white !important;
&:hover {
  background-color: ${props => color(props.color).darken(10).toRgbString()} !important;
}
`

function getParams(obj) {
  const params = Object.entries(obj)
        .filter(([, value]) => value !== undefined && value !== null)
        .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(String(value))}`);
  return params.length > 0 ? `?${params.join('&')}` : '';
}

const OWN_QUOTE = "I entered the VicPAH 2020 Photo Competition! Take a look!";
const OTHER_QUOTE = "I really love this photo in the VicPAH 2020 Photo Competition! Take a look!";


class ShareButton extends PureComponent {
  handleClick = () => {
    const opts = {
      height: 400,
      width: 550,
      location: 'no',
      toolbar: 'no',
      status: 'no',
      directories: 'no',
      menubar: 'no',
      scrollbars: 'yes',
      resizable: 'no',
      centerscreen: 'yes',
      chrome: 'yes',
    };
    window.open(
      this.props.href,
      '',
      Object.entries(opts).map(([k,v]) => `${k}=${v}`).join(','),
    );
  }
  renderIcon = () => {
    const { icon, network } = this.props;
    if (_.isString(icon)) {
      return <Icon name={ icon } />
    } else if (_.isNil(icon)) {
      return <Icon name={ network } />
    } else {
      return icon;
    }
  }
  render() {
    const { network, color, label } = this.props;
    const ButtonComponent = color && (color.startsWith('#') || color.startsWith('rgb'))
          ? ShareButtonColored
          : Button
    return (
      <ButtonComponent color={ color || network } onClick={ this.handleClick }>
        { this.renderIcon() } { label || _.upperFirst(network) }
      </ButtonComponent>
    )
  }
}
class FacebookShareButton extends PureComponent {
  getUrl = () => 'https://www.facebook.com/sharer/sharer.php' + getParams({
    u: this.props.url || document.location.toString(),
    quote: this.props.own
      ? OWN_QUOTE
      : OTHER_QUOTE,
    hashtag: '#notMyDayJob',
  })
  render() {
    return <ShareButton network="facebook" href={ this.getUrl() }/>
  }
}
class TwitterShareButton extends PureComponent {
  getUrl = () => 'https://twitter.com/share' + getParams({
    url: this.props.url || document.location.toString(),
    text: this.props.own
      ? OWN_QUOTE
      : OTHER_QUOTE,
    hashtags: 'notMyDayJob,VicPAH,puppyplay',
  })
  render() {
    return <ShareButton network="twitter" href={ this.getUrl() }/>
  }
}
class WhatsappShareButton extends PureComponent {
  isDevice = () => /(android|iphone|ipad|mobile)/i.test(navigator.userAgent);
  getUrl = () => 'https://' + (this.isDevice() ? 'api' : 'web') + '.whatsapp.com/send' + getParams({
    text: (
      this.props.own
        ? OWN_QUOTE
        : OTHER_QUOTE
    ) + '\n' + (this.props.url || document.location.toString()),
  })
  render() {
    return <ShareButton network="whatsapp" color="#25D366" href={ this.getUrl() }/>
  }
}
class TelegramShareButton extends PureComponent {
  getUrl = () => 'https://telegram.me/share/' + getParams({
    url: this.props.url || document.location.toString(),
    text: this.props.own
      ? OWN_QUOTE
      : OTHER_QUOTE,
  })
  render() {
    return <ShareButton network="telegram" icon="telegram plane" color="rgb(82,197,255)" href={ this.getUrl() }/>
  }
}

const SHARE_BUTTONS = [
  FacebookShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  TelegramShareButton,
];

class IndexPage extends PureComponent {
  handleClickEnter = () => {
    const { history, match } = this.props;
    history.push(`${match.path}enter/`);
  }
  handleClickVote = () => {
    const { history, match } = this.props;
    history.push(`${match.path}vote-pairs/`);
  }
  render() {
    return (
      <Fragment>
        <h1>VicPAH Photo Competition 2020</h1>
        <div style={{ position: 'relative' }}>
          <Rail position="right">
            <h4>Sponsor:</h4>
            <a href="https://www.eagleleather.com.au">
              <img src={ eagleLogo } width="200px" alt="Eagle Leather"/>
            </a>
          </Rail>
          <p>Puppies, Handlers and everyone in between.</p>
          <p>As you know pup play is a great way to help with your mental health especially through difficult times.</p>
          <p>With that in mind, we’d like to give you one more reason to put on a hood, grab a leash, or do whatever it is you do to have fun and show the world how they can join you from their own homes:</p>
          <p>We are hosting a photo contest with the winner getting an Eagle Leather voucher to the value of $50!</p>
          <p>To enter simply send us your photo/s to be shared along with your pup name through the link below.</p>
          <p>Finalists will be chosen via voting on our website. The winner will be notified via email and published on both our website & social media accounts.</p>

          <p><strong>The theme for our first ever photo competition is</strong></p>
          <Segment><Header style={{ textAlign: 'center' }}>Not my day job</Header></Segment>
          <p><strong>Entry, and voting will close at</strong></p>
          <Segment><Header style={{ textAlign: 'center' }}>Sunday 19th April 2020 at midnight, AEST</Header></Segment>
          <p>The winner will be announced a few days after voting close, once we have had time to consider the finalists.</p>

          <h2>Points to consider</h2>
          <ul>
            <li>Puppy play is a great way to relax, don’t forget about it when it comes to dealing with stress</li>
            <li>You’re a part of a global, caring community</li>
            <li>Spread new ideas for things people can do as pup/handler without going out into the wider community</li>
            <li>It's up to you how you choose to interpret the theme, however the winner will be chosen based on how accurately the chosen photo fulfills the theme and the above points, so ensure that the intent is obvious</li>{/*'*/}
            <li>There are some <MaybeLink to="/photo-comp-2020/terms/">terms and conditions</MaybeLink>, so make sure you follow them!</li>
          </ul>
          <h2>The competition is closed</h2>
          {/*<div style={{ display: 'flex', justifyContent: 'center' }}>
            <Button.Group size="massive" widths="2">
              <Button onClick={ this.handleClickEnter } positive><Icon name="photo" /> Enter</Button>
              <Button.Or />
              <Button onClick={ this.handleClickVote }><Icon name="arrow up" /> Vote</Button>
            </Button.Group>
          </div>*/}
        </div>
      </Fragment>
    );
  }
}

const ENTRY_SUBMIT_MESSAGES = {
  waiting: "This shouldn't take long",
  preparing: ':paw_prints: Just a sec...',
  uploading: ':rocket: Sending your pic to the cloud!',
  processing: ':robot: *whirrs and buzzes*',
  complete: 'DING!',
  error: "Something's gone awry!",
};
const ENTRY_SUBMIT_ORDER = [
  'waiting', 'preparing', 'uploading', 'processing', 'complete',
];

@withRouter
class EntrySubmit extends PureComponent {
  state = {
    step: 'waiting',
    processingTimer: null,
    slow: false,
    slowTimer: null,
  }
  componentDidMount() {
    const { photoHash, photoInput } = this.props;

    const file = photoInput.files[0];
    const fileParts = file.name.split('.');

    this.setStep('preparing', 45000);

    getApi().post(
      `/upload`,
      {
        hash: this.props.photoHash,
        extension: fileParts[fileParts.length - 1],
        ..._.pick(this.props, ['name', 'email', 'caption'])
      },
    )
      .then(resp => {
        this.setStep('uploading', 30000);
        const formData = new FormData();
        for (const [key, value] of Object.entries(resp.data.fields)) {
          formData.append(key, value);
        }
        formData.append('file', file);
        return axios.post(resp.data.url, formData);
      })
      .then(resp => {
        addOwned(photoHash);
        addVote(photoHash);
        this.setStep('processing', 20000);
        this.setState({ processingTimer: setTimeout(this.checkForProcessed, 3000) });
      })
      .catch(err => {
        console.error(err)
        this.setStep('error');
        if (err.response && err.response.data) {
          if (err.response.data.error) {
            this.setState({ error: err.response.data.error });
          } else if (err.response.data.message) {
            this.setState({ error: err.response.data.message });
          }
        }
      });
  }

  setStep = (step, timeout) => {
    this.clearTimeoutSlow();
    this.setState({
      step: step,
      slow: false,
      slowTimer: timeout ? setTimeout(this.timeoutSlow, timeout) : null,
    });
  }
  clearTimeoutSlow = () => {
    clearTimeout(this.state.slowTimer);
  }
  timeoutSlow = () => {
    this.setState({ slow: true });
  }

  checkForProcessed = async () => {
    const { photoHash } = this.props;
    try {
      await axios.head(getPhotoUrl(photoHash, 700));
      this.setStep('complete', 5000);
      this.props.history.push(getVoteUrlPath(photoHash));
    } catch(err) {
      this.setState({
        processingTimer: setTimeout(this.checkForProcessed, 1000)
      });
    }
  }

  render() {
    const { photoHash } = this.props;
    const { slow, step, error } = this.state;
    const stepMsg = ENTRY_SUBMIT_MESSAGES[step];

    if (step === 'error') {
      return (
        <Message error>
          <Message.Header>{ stepMsg }</Message.Header>
          { error && <p>{ error }</p> }
        </Message>
      )
    }

    const stepIdx = ENTRY_SUBMIT_ORDER.indexOf(step);
    return (
      <Progress percent={ (stepIdx+1)/5*100 } success={ !slow } warning={ !!slow }>
        <div>{ emojify(stepMsg) }</div>
        { slow && (
          <Message warning>
            <Message.Header>Hmm this is taking longer than expected</Message.Header>
            <p>There might be something wrong! If you'd like email your entry, you can send your photo and the details you submitted on the previous page, along with this reference: <Label>{ photoHash }</Label> to biru@vicpah.org.au</p>
            {/*' fixes a syntax highlight bug*/}
          </Message>
        ) }
      </Progress>
    );
  }
}
// TODO export added to avoid not used lint issues
export class EntryPage extends PureComponent {
  state = {
    nameValue: '',
    nameError: null,
    emailValue: '',
    emailError: null,
    captionValue: '',
    captionError: null,
    agreeValue: false,
    agreeError: null,
    photoValue: null,
    photoError: null,
    photoHash: null,
    submitting: false,
  }
  getAllErrors(newState={}) {
    const { nameValue, emailValue, captionValue, agreeValue, photoValue } = {
      ...this.state,
      ...newState,
    };

    let name = null;
    let email = null;
    let caption = null;
    let agree = null;
    let photo = null;

    if (!nameValue || !nameValue.trim()) {
      name = 'Required field';
    } else if (nameValue.length > 200) {
      name = 'Must be less than 200 characters';
    }

    if (!emailValue || !emailValue.trim()) {
      email = 'Required field';
    } else if (emailValue.indexOf('@') === -1 || emailValue.indexOf('.') === -1) {
      email = 'Must be a valid email address';
    } else if (emailValue.length > 200) {
      email = 'Must be less than 200 characters';
    }

    if (!captionValue || !captionValue.trim()) {
      caption = 'Required field';
    } else if (captionValue.length > 200) {
      caption = 'Must be less than 200 characters';
    }

    if (!photoValue) {
      photo = 'Required field';
    } else if (!photoValue.type || !photoValue.type.startsWith('image/')) {
      photo = 'Must be an image';
    }

    if (!agreeValue) {
      agree = 'Must agree to T&Cs';
    }

    return { name, email, caption, agree, photo };
  }
  checkAllValid = () => {
    const errors = this.getAllErrors()
    this.setState(_.mapKeys(errors, (v,k) => `${k}Error`));
    return Object.values(errors).every(msg => !msg)
  }
  handleClickUpload = () => {
    this.refs.photo.click();
  }
  handleChangeUpload = ev => {
    const file = ev.target.files[0];

    const { photo: error } = this.getAllErrors({ photoValue: file });
    this.setState({ photoValue: file, photoError: error });

    if (error) {
      return;
    }

    const hash = new BLAKE2s(32);
    const reader = new FileReader();
    reader.onload = loadEv => {
      hash.update(new Uint8Array(loadEv.target.result));
      this.setState({ photoHash: hash.hexDigest() });
    };
    reader.readAsArrayBuffer(file);
  }
  handleSubmit = ev => {
    const valid = this.checkAllValid();
    if (valid) {
      this.setState({ submitting: true });
    }
  }

  handleFieldChange = (ev, name, getValue=ev => ev.target.value) => {
    const value = getValue(ev);
    const errors = this.getAllErrors({ [`${name}Value`]: value });
    const error = errors[name];
    console.log({ value, error })
    this.setState({
      [`${name}Value`]: value,
      [`${name}Error`]: error,
    })
  }
  handleNameChange = ev => this.handleFieldChange(ev, 'name');
  handleEmailChange = ev => this.handleFieldChange(ev, 'email');
  handleCaptionChange = ev => this.handleFieldChange(ev, 'caption')
  handleAgreeChange = ev => this.handleFieldChange(ev, 'agree', () => !this.state.agreeValue);

  render() {
    const {
      nameError, nameValue,
      emailError, emailValue,
      captionError, captionValue,
      photoError, photoValue, photoHash,
      agreeError, agreeValue,
      submitting,
    } = this.state;
    return (
      <Fragment>
        <EnsureToken />
        <h1>VicPAH Photo Competition Entry</h1>
        <Form
          size="massive"
          onSubmit={ this.handleSubmit }
          style={{ display: submitting ? 'none' : 'inherit' }}
          >
          <Form.Field error={ !!nameError } required>
            <label>Your scene/preferred name</label>
            <Input
              placeholder="Pup ..., Handler .."
              onChange={ this.handleNameChange }
              value={ nameValue }
              />
            <FormHelp>Will appear above to your submission</FormHelp>
            { nameError && <FormError>{ nameError }</FormError> }
          </Form.Field>
          <Form.Field error={ !!emailError } required>
            <label>Your email address</label>
            <Input
              placeholder="many_barks@example.com"
              onChange={ this.handleEmailChange }
              value={ emailValue }
              />
            { emailError && <FormError>{ emailError }</FormError> }
          </Form.Field>
          <Form.Field error={ !!captionError } required>
            <label>Photo caption</label>
            <Input
              placeholder="Just pupper things..."
              onChange={ this.handleCaptionChange }
              value={ captionValue }
              />
            <FormHelp>Will appear above to your submission</FormHelp>
            { captionError && <FormError>{ captionError }</FormError> }
          </Form.Field>
          <Form.Field error={ !!photoError } required>
            <label>Your photo</label>
            <input
              type="file"
              ref="photo"
              accept="image/*"
              style={{ display: 'none' }}
              onChange={ this.handleChangeUpload }
              />
            <Input
              value={ photoValue ? photoValue.name : '' }
              placeholder="Select a file..."
              onClick={ this.handleClickUpload }
              action={{ content: 'select', icon: 'upload', onClick: this.handleClickUpload }}
              readonly
              />
            { photoError && <FormError>{ photoError }</FormError> }
          </Form.Field>
          <Form.Field error={ !!agreeError } required>
            <Checkbox
              label="I agree to the Terms and Conditions"
              onChange={ this.handleAgreeChange }
              checked={ agreeValue }
              />
            <FormHelp size="0.5em" style={{ marginLeft: '26px' }}>You can <MaybeLink to="/photo-comp-2020/terms/">read the T&Cs here</MaybeLink></FormHelp>
            { agreeError && <FormError>{ agreeError }</FormError> }
          </Form.Field>
          <Button
            size="massive"
            type="submit"
            color="positive"
            style={{ width: '100%' }}
            >
            Submit your entry!
          </Button>
        </Form>
        { submitting && (
          <EntrySubmit
            name={ nameValue }
            email={ emailValue }
            caption={ captionValue }
            photoHash={ photoHash }
            photoInput={ this.refs.photo }
            />
        ) }
      </Fragment>
    );
  }
}

const ImageCompTitle = styled.h1`
text-align: center;
`
const ImageCaption = styled.h2`
text-align: center;
`
const ImageOwner = styled.h3`
text-align: center;
`

class PhotoSingle extends PureComponent {
  state = {
    loading: true,
    loadError: false,
    photoData: null,
  }
  componentDidMount() {
    getApi()
      .get(`/getphoto/${this.props.hash}`)
      .then(resp => {
        this.setState({ loading: false, photoData: resp.data });
      })
      .catch(err => {
        console.error('Error loading photo data', err);
        this.setState({ loading: false, loadError: true });
      });
  }
  render() {
    const { hash } = this.props;
    const { loading, loadError, photoData } = this.state;
    return (
      <Fragment>
        { loadError && <Message error>Something's gone awry! Try again later</Message>/*'*/ }
        { loading
          ? <Loader active inline='centered' size="massive">Loading photo data</Loader>
          : !loadError && (
            <Fragment>
              <ImageCaption>{ photoData.caption }</ImageCaption>
              <ImageOwner>{ photoData.name }</ImageOwner>
              <Image src={ getPhotoUrl(hash, '700') } centered />
            </Fragment>
          )
        }
      </Fragment>
    );
  }
}
const VotePairsFrame = styled.div`
display: flex;
min-height: 200px;
`;
const VotePairsSingleWrap = styled(Dimmer.Dimmable)`
cursor: pointer;
`;
class VotePairsSingle extends PureComponent {
  state = {
    hover: false,
  }

  handleEnter = () => this.setState({ hover: true })
  handleLeave = () => this.setState({ hover: false })

  render() {
    const { hash } = this.props;
    const { hover } = this.state;
    return (
      <div style={{ flexGrow: 1, display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
        <VotePairsSingleWrap
          dimmed={ hover }
          onMouseEnter={ this.handleEnter }
          onMouseLeave={ this.handleLeave }
          onClick={ this.props.onClick }
          >
          <PhotoSingle hash={ hash } />
          <Dimmer active={ hover } inverted>
            <div style={{ fontSize: '30px', color: 'red' }}><Icon name='heart' /> I like this one!</div>
          </Dimmer>
        </VotePairsSingleWrap>
        <div style={{ paddingTop: '20px', textAlign: 'center' }}>
          <h4 style={{ display: 'inline', paddingRight: '20px' }}>Share:</h4>
          { SHARE_BUTTONS.map((Component, idx) => (
            <Component key={ idx } url={ getVoteUrl(hash) } own={ checkOwned(hash) }/>
          )) }
        </div>
      </div>
    );

  }
}
// TODO export added to avoid not used lint issues
export class VotePairsPage extends PureComponent {
  state = {
    needToken: !getToken(),
    loading: false,
    errorDetail: null,
  }
  componentDidMount() {
    if (this.state.needToken) {
      return;
    }
    this.loadRace();
  }
  loadRace = () => {
    this.setState({ loading: true, loadError: false, errorDetail: null });
    getApi()
      .post('/voteraceget')
      .then(resp => this.setState({ loading: false, hashes: resp.data.hashes }))
      .catch(err => {
        console.error('Error loading race', err);
        this.setState({
          loading: false,
          loadError: true,
          errorDetail: err.response ? err.response.data : null,
        });
      })
  }
  voteFor = async hash => {
    this.setState({ hashes: null });
    try {
      await getApi().post('/voterace', { hash });
    } finally {
      this.loadRace();
    }
  }
  handleClickLeft = () => {
    this.voteFor(this.state.hashes[0]);
  }
  handleClickRight = () => {
    this.voteFor(this.state.hashes[1]);
  }
  renderError = () => {
    const { loadError, errorDetail } = this.state;
    if (errorDetail && errorDetail.error_code) {
      return (
        <Fragment>
          { errorDetail.error_code === 'not_enough' && (
            <Message
              icon="clock"
              header="Not enough photos have been submitted yet"
              content="We need a few more first. Check back later"
              error />
          ) }
          { errorDetail.error_code === 'no_more' && (
            <Message
              icon="inbox"
              header="No more pairs left"
              content="You've worked through all the photo pairs that we have right now! Check back later!"
              error />
          ) }
        </Fragment>
      );
    } else if (errorDetail && errorDetail.error) {
      return <Message error>{ errorDetail.error }</Message>;
    } else if (errorDetail && errorDetail.message) {
      return <Message error>{ errorDetail.message }</Message>;
    } else if (loadError) {
      return <Message error>There was an issue loading your next pair</Message>;
    }
    return null;
  }
  render() {
    const { loading, hashes, needToken } = this.state;

    return (
      <Fragment>
        <Container className="content" text>
          { needToken && <EnsureToken onGotToken={ this.loadRace }/> }
          <ImageCompTitle>VicPAH Photo Competition</ImageCompTitle>
          <p style={{ textAlign: 'center' }}>Click the photo you like the most!</p>
          { this.renderError() }
          { loading && <Loader active inline='centered' size="massive">Loading a new pair</Loader> }
        </Container>
        { !loading && hashes && (
          <VotePairsFrame>
            <VotePairsSingle hash={ hashes[0] } onClick={ this.handleClickLeft } />
            <VotePairsSingle hash={ hashes[1] } onClick={ this.handleClickRight } />
          </VotePairsFrame>
        ) }
      </Fragment>
    )
  }
}
class VoteForPage extends PureComponent {
  state = {
    loading: true,
    voting: false,
    voted: false,
    needToken: false,
  }
  componentDidMount() {
    const { match: { params: { photoHash } } } = this.props;
    this.setState({ voted: checkVote(photoHash) });
    getApi()
      .get(`/getphoto/${photoHash}`)
      .then(resp => {
        this.setState({ loading: false, photoData: resp.data });
      })
      .catch(err => {
        console.error('Error loading photo data', err);
        this.setState({ loading: false, loadError: true });
      });
  }
  handleVoteClick = () => {
    const { match: { params: { photoHash } } } = this.props;
    if (!getToken()) {
      this.setState({ needToken: true });
    } else {
      this.setState({ voting: true });
      getApi()
        .post(`/voteone/${photoHash}`)
        .then(resp => {
          addVote(this.props.match.params.photoHash);
          this.setState({ voted: true, voting: false });
        })
        .catch(err => {
          console.error('Vote error', err);
          this.setState({ showVoteError: true, voting: false });
          setTimeout(() => this.setState({ showVoteError: false }), 2000);
        });
    }
  }
  render() {
    const { match: { params: { photoHash } } } = this.props;
    const { needToken, voted, loading, loadError, photoData, voting, showVoteError } = this.state;
    return (
      <Fragment>
        { needToken && <EnsureToken onGotToken={ this.handleVoteClick }/> }
        <ImageCompTitle>VicPAH Photo Competition</ImageCompTitle>
        { loadError && <Message error>Something's gone awry! Try again later</Message>/*'*/ }
        { loading
          ? <Loader active inline='centered' size="massive">Loading the photo</Loader>
          : !loadError && (
            <Fragment>
              <ImageCaption>{ photoData.caption }</ImageCaption>
              <ImageOwner>{ photoData.name }</ImageOwner>
              <Segment>
                <Image src={ getPhotoUrl(photoHash, '700') } centered />
                <Rail position="right">
                  <Button
                    color="red"
                    labelPosition="left"
                    disabled={ true }
                    onClick={ this.handleVoteClick }
                    icon
                    >
                    <Icon name={ voting ? 'spinner' : (voted ? 'check' : 'heart') } loading={ voting } /> Vote for this photo
                  </Button>
                  <p>Voting is closed</p>
                  <h4>Share:</h4>
                  { SHARE_BUTTONS.map((Component, idx) => (
                    <p key={ idx }>
                      <Component own={ checkOwned(photoHash) }/>
                    </p>
                  )) }
                </Rail>
              </Segment>
            </Fragment>
          )
        }
        <Portal open={ showVoteError }>
          <Message error>
            <Message.Header>Something went awry</Message.Header>
            <p>For some reason, we couldn't submit your vote! Try again later</p>{/*'*/}
          </Message>
        </Portal>
      </Fragment>
    );
  }
}

class TermsPage extends PureComponent {
  render() {
    return (
      <Fragment>
        <h1>VicPAH Photo Competition Entry Conditions</h1>
        <ul>
          <li>Entries should be submitted via the website form, files no larger than 20mb.</li>
          <li>Photos should be appropriate for social media, and safe for work.</li>
          <li>Entrants must be visible in the photo. You must have permission of any persons/subjects in the photo, and photographer.</li>
          <li>Photographs should have been taken in the last two years, with no more than light photo editing/filters.</li>
          <li>Entries from persons under 18 at 30 April 2020 must have signed parent/guardian consent.</li>
          <li>Decision of competition winner will be final, with no correspondence.</li>
          <li>By submitting the permission form, the entrant grants permission to VicPAH to copy and publish their submission (including any photograph or text) or part of it for any purpose relating to the VicPAH's photo competitions. VicPAH may publish your work on our websites, social media sites or in print or digital publication. Photographs will be credited wherever the images are used. Copyright and intellectual property of the images remains the permission of the photographer.</li>{/*'*/}
          <li>Submitting the entry online constitutes acceptance of these conditions.</li>
          <li>Votes are limited to 1 per photo, per person. Circumventing anti-robot features, or otherwise participating in "synthetic interaction" with this site may result in the committee taking disciplinary action.</li>
        </ul>
      </Fragment>
    );
  }
}

class ClosedPage extends PureComponent {
  render() {
    return <Message error>The competition has closed.</Message>;
  }
}
export class PhotoComp2020Route extends PureComponent {
  render() {
    const { match } = this.props;
    return (
      <Switch>
        <Container className="content" text>
          <Route path={ match.path } component={ IndexPage } exact />
          <Route path={ `${match.path}vote-pairs/` } component={ ClosedPage } exact />
          <Route path={ `${match.path}terms/` } component={ TermsPage } exact />
          <Route path={ `${match.path}enter/` } component={ ClosedPage } exact />
          <Route path={ `${match.path}vote-for/:photoHash` } component={ VoteForPage } exact />
        </Container>
      </Switch>
    );
  }
}
