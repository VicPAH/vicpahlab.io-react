import yaml from 'js-yaml';
import { PureComponent } from 'react';

import { getMarkdownParser, getMarkdownToReact } from '../../utils/markdown'


export class MarkdownPage extends PureComponent {
  state = {
    content: null,
    meta: null,
  }
  componentDidMount() {
    this.parseResponse()
  }
  componentDidUpdate(prevProps) {
    if (this.props.response.data !== prevProps.response.data) {
      this.parseResponse()
    }
  }
  parseResponse = () => {
    const { response: { data }, onMeta } = this.props
    const mdp = getMarkdownParser()
      .use(() => ast => {
        if (!onMeta) {
          return;
        }
        const yamlAst = ast.children.find(childAst => childAst.type === 'yaml')
        if (yamlAst) {
          onMeta(yaml.safeLoad(yamlAst.value))
        }
      });
    getMarkdownToReact(mdp)
      .process(
        data,
        (err, file) => {
          if (err) {
            console.error(err);
          }
          if (file) {
            this.setState({ content: file.contents })
          }
        }
      )
  }
  render() {
    return this.state.content
  }
}
