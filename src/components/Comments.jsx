import commentBox from 'commentbox.io'
import React, { PureComponent } from 'react'


const COMMENTBOX_PROJECT = '5729577624141824-proj'


export class Comments extends PureComponent {
  componentDidMount() {
    this.removeCommentBox = commentBox(
      COMMENTBOX_PROJECT,
    )
  }
  componentWillUnmount() {
    this.removeCommentBox()
  }

  render() {
    return <div className="commentbox" />
  }
}
