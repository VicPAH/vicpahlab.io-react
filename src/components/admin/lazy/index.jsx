import React from 'react';
import { Route, Switch } from 'react-router';

import { MembersPage } from './MembersPage';
import { EmailSigPage } from './EmailSigPage';

export const AdminRoute = props => {
  return (
    <Switch>
      <Route path={ `${props.match.path}emailsig` } component={ EmailSigPage } />
      <Route path={ `${props.match.path}members` } component={ MembersPage } />
    </Switch>
  )
};
