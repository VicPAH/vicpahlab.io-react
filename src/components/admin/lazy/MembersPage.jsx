import _ from 'lodash';
import React, { Fragment, useCallback, useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { toast } from 'react-semantic-toasts';
import { Button, Dropdown, Icon, Popup, Rail, Table } from 'semantic-ui-react';
import styled, { css } from 'styled-components/macro';

import { StateInputModal } from './StateInputModal';
import { stateLogToMemberSvc } from '../../../utils/memberSvc';

import * as s from '../../../store/selectors';

const MemberTable = styled(Table)`
& textarea, & input {
  height: 100%;
  width: 100%;
  border: none;
  display: block;
}
`;

const MonoStyle = css`
font-family: monospace;
`;
/*const PreStyle = css`
white-space: pre;
`;*/

const MonoText = styled.span`
${MonoStyle}
`;
/*const PreText = styled.span`
${PreStyle}
`;
const MonoPreText = styled.span`
${MonoStyle}
${PreStyle}
`;*/
const UnspecifiedText = styled.span.attrs({ children: 'Unspecified' })`color: #aaa`;

const TooltipIcon = props => (
  <Popup { ..._.pick(props, ['content', 'header', 'position'])}
         trigger={ <Icon { ..._.omit(props, ['content', 'header', 'position']) } /> }
         />
);

const getMemberFlags = ({ stateLog, stateSvc, scope, joinDate }) => ({
  isNoMember: stateSvc.state.matches('noMember'),
  isMember: stateSvc.state.matches('member'),
  isExpelled: stateSvc.state.matches('expelled'),
  isApplied: stateSvc.state.matches('applied'),
  canPay: stateSvc.state.nextEvents.indexOf('PAY') >= 0,
  isCommittee: (scope || []).indexOf('committee') >= 0,
  isUser: !!joinDate,
  isHistoricMember: (
    stateLog.map(l => l.action).indexOf('APPROVE') !== -1 &&
    stateLog.map(l => l.action).indexOf('PAY') !== -1
  ),
})

const MemberRow = withRouter(props => {
  const { actionInProgress, history, id, onAddLogClick, onMergeClick, stateLog, stateSvc, isSecretaryScope } = props;
  const handleOpen = useCallback(() => {
    history.push(`/members/${id}`);
  }, [id, history]);
  const handleMergeClick = useCallback(() => {
    onMergeClick({ id });
  }, [id, onMergeClick]);
  const handleAddLogClick = useCallback(() => {
    onAddLogClick({ id, stateLog, stateSvc });
  }, [id, onAddLogClick, stateLog, stateSvc]);

  const {
    isNoMember,
    isMember,
    isExpelled,
    isApplied,
    canPay,
    isCommittee,
    isUser,
  } = getMemberFlags(props);

  return (
    <Fragment>
      <Table.Row positive={ isMember } negative={ isExpelled } warning={ isApplied }>
        <Table.HeaderCell collapsing>
          <Button onClick={ handleOpen } icon basic><Icon name="external" /></Button>&nbsp;
          <MonoText>{ props.id }</MonoText>&nbsp;
          { isUser && <TooltipIcon content="Registered user" name="user" /> }
          { isApplied && <TooltipIcon content="Applied for membership" name="file alternate" color="yellow" /> }
          { canPay && !isNoMember && <TooltipIcon content="Payment due" name="dollar" color="yellow" /> }
          { isCommittee && <TooltipIcon content="Committee member" name="handshake" color="blue" /> }
        </Table.HeaderCell>
        <Table.Cell>{ props.displayName || <UnspecifiedText /> }</Table.Cell>
        <Table.Cell>{ props.givenName || <UnspecifiedText /> }</Table.Cell>
        <Table.Cell>{ props.familyName || <UnspecifiedText /> }</Table.Cell>
        { isSecretaryScope && <Table.Cell>
          { props.email || <UnspecifiedText /> }&nbsp;
          { props.emailVerified && <TooltipIcon content="Verified" name="check" color="green" /> }
        </Table.Cell> }
        { isSecretaryScope && <Table.Cell collapsing>
          <Button onClick={ handleMergeClick } disabled={ actionInProgress }>Merge/Delete</Button>
          <Button onClick={ handleAddLogClick } disabled={ actionInProgress }>Add log</Button>
        </Table.Cell> }
      </Table.Row>
    </Fragment>
  );
});

const allFilters = [
  'Members',
  'Applied',
  'Payment due',
  'Expelled',
  'Historic members',
];

export const MembersPage = connect(
  state => ({
    refreshClient: state.refreshClient,
    isSecretaryScope: s.isSecretaryScope(state),
  })
)(props => {
  const { refreshClient, isSecretaryScope } = props;
  const [rawData, setData] = useState(null);
  const data = useMemo(
    () => rawData
      ? rawData.map(member => ({
        ...member,
        stateSvc: stateLogToMemberSvc(member.stateLog, member.id)
      }))
      : null,
    [rawData],
  );
  useEffect(() => {
    if (!refreshClient) return;
    if (rawData) return;
    refreshClient
      .get('/member_admin')
      .then(resp => setData(resp.data));
  }, [rawData, refreshClient]);

  const [adding, setAdding] = useState(false);
  const handleAdd = useCallback(() => {
    if (!refreshClient) return;
    setAdding(true);
    refreshClient
      .post('/member_admin', { action: 'create' }, { maxRedirects: 0 })
      .then(resp => {
        setAdding(false);
        props.history.push(`/members/${resp.data.id}`);
      })
      .catch(err => {
        setAdding(false);
        toast({ title: 'Error adding new member', type: 'error' });
        console.error(err);
      });
  }, [refreshClient, props.history]);
  const [merging, setMerging] = useState(false);
  const handleMergeClick = useCallback(o => {
    if (!refreshClient) return;
    setMerging(true);
    const deleteId = o.id;
    const deleteObj = data.find(m => m.id === deleteId);
    const primaryId = prompt('ID of the new primary?');
    const primaryObj = data.find(m => m.id === primaryId);

    if (!primaryObj) {
      toast({ title: `Member with ID ${JSON.stringify(primaryId)} not found`, type: 'error' });
      setMerging(false);
      return;
    }

    // eslint-disable-next-line no-restricted-globals
    const proceed = confirm([
      'TO BE UPDATED:',
      `Display name: ${JSON.stringify(primaryObj.displayName)}`,
      `Email: ${JSON.stringify(primaryObj.email)}`,
      `Given name: ${JSON.stringify(primaryObj.givenName)}`,
      `Family name: ${JSON.stringify(primaryObj.familyName)}`,
      '',
      'TO BE DELETED:',
      `Display name: ${JSON.stringify(deleteObj.displayName)}`,
      `Email: ${JSON.stringify(deleteObj.email)}`,
      `Given name: ${JSON.stringify(deleteObj.givenName)}`,
      `Family name: ${JSON.stringify(deleteObj.familyName)}`,
    ].join('\n'));
    if (proceed) {
      refreshClient
        .post(
          '/member_admin',
          { action: 'merge', primaryId, deleteId },
        )
        .then(resp => {
          setMerging(false);
          setData(null);
        })
        .catch(err => {
          setMerging(false);
          toast({ title: 'Error merging members', type: 'error' });
          console.error(err);
        });
    } else {
      setMerging(false);
    }
  }, [data, refreshClient]);

  const [addLogMember, setAddLogMember] = useState(null);
  const handleAddLogClick = useCallback(o => {
    setAddLogMember(o);
  }, [setAddLogMember]);
  const handleAddLogClose = useCallback(() => {
    setAddLogMember(null);
  }, [setAddLogMember]);
  const handleAddLogUpdate = useCallback(newData => {
    setData(rawData.map(
      member => member.id === newData.id
        ? newData
        : member
    ));
  }, [rawData, setData]);

  const [filter, setFilter] = useState(null);
  const handleFilterChanged = useCallback((ev, data) => {
    setFilter(data.value);
  }, [setFilter]);

  const finalData = useMemo(
    () => (
      data
        ? data.filter(m => {
          const {
            isNoMember,
            isMember,
            isExpelled,
            isApplied,
            canPay,
            isHistoricMember,
          } = getMemberFlags(m);
          switch (filter) {
            case 'Members':
              return isMember;
            case 'Applied':
              return isApplied;
            case 'Payment due':
              return canPay && !isNoMember;
            case 'Expelled':
              return isExpelled;
            case 'Historic members':
              return isHistoricMember;
            default:
              return true;
          }
        })
      : data
    ),
    [data, filter]
  );

  useEffect(() => {
    window._vicpahMembers = data;
    window._vicpahMembersShown = finalData;
    return () => {
      delete window._vicpahMembers;
      delete window._vicpahMembersShown;
    }
  }, [data, finalData]);

  if (!data) return 'Loading...';
  const inProgress = adding || merging;

  return (
    <Fragment>
      <div style={{ position: 'relative', textAlign: 'right' }}>
        <Rail position="right" internal>
          <span style={{ paddingRight: '10px' }}>{ finalData.length }</span>
          <Button.Group>
            <Dropdown
              icon="filter"
              className="icon"
              options={[
                {key: 'clear', text: 'Clear', value: null},
                ...allFilters.map(text => ({ text, key: text, value: text })),
              ]}
              onChange={ handleFilterChanged }
              value={ filter }
              floating
              button
            />
            <Button onClick={ handleAdd } loading={ adding } disabled={ inProgress } icon positive><Icon name="plus" /></Button>
          </Button.Group>
        </Rail>
      </div>
      <StateInputModal
        { ...(addLogMember || {}) }
        key={ (addLogMember || {}).id || 'none' }
        open={ !!addLogMember }
        onClose={ handleAddLogClose }
        onUpdate={ handleAddLogUpdate }
        />
      <MemberTable compact="very" singleLine selectable>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell collapsing>ID</Table.HeaderCell>
            <Table.HeaderCell>Scene Name</Table.HeaderCell>
            <Table.HeaderCell>Given Name</Table.HeaderCell>
            <Table.HeaderCell>Family Name</Table.HeaderCell>
            { isSecretaryScope && <Table.HeaderCell>Email</Table.HeaderCell> }
          </Table.Row>
        </Table.Header>
        <Table.Body>
          { finalData.map(m => (
            <MemberRow
              key={ m.id }
              onAddLogClick={ handleAddLogClick }
              onMergeClick={ handleMergeClick }
              actionInProgress={ inProgress }
              isSecretaryScope={ isSecretaryScope }
              { ...m }
              />
          )) }
        </Table.Body>
      </MemberTable>
    </Fragment>
  );
});
