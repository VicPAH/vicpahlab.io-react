import React, { useCallback, useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { Button, Container, Form, Icon, Segment } from 'semantic-ui-react';
import { withTheme } from 'styled-components';

import * as s from '../../../store/selectors';

/* global ClipboardItem */

export const EmailSigPage = withTheme(connect(state => ({
  loggedIn: s.getLoggedIn(state),
}))(props => {
  const { loggedIn, theme } = props;
  const { displayName: displayNameDefault } = (loggedIn || {});

  const [displayName, setDisplayName] = useState(displayNameDefault);
  const [position, setPosition] = useState('Committee Member');
  const [salutation, setSalutation] = useState('Thanks!');

  const copyRef = useRef(null);

  useEffect(() => {
    if (displayNameDefault) setDisplayName(displayNameDefault);
  }, [displayNameDefault, setDisplayName]);
  const handleChange = useCallback((ev, { name, value }) => {
    ({
      name: setDisplayName,
      position: setPosition,
      salutation: setSalutation,
    }[name])(value);
  }, [setDisplayName, setPosition, setSalutation]);
  const handleCopy = useCallback(async () => {
    const el = copyRef.current;
    await navigator.clipboard.write([
      new ClipboardItem({
        'text/html': new Blob([el.innerHTML], { type: 'text/html' }),
      }),
    ]);
  }, [copyRef]);

  return (
    <Container className="content" text>
      <Form>
        <Form.Input
          label="Salutation"
          name="salutation"
          value={ salutation }
          onChange={ handleChange }
        />
        <Form.Input
          label="Scene name"
          name="name"
          value={ displayName }
          onChange={ handleChange }
        />
        <Form.Dropdown
          label="Committee position"
          name="position"
          value={ position }
          options={ [
            { key: 'member', text: 'Committee Member', value: 'Committee Member' },
            { key: 'president', text: 'President', value: 'President' },
            { key: 'vice-president', text: 'Vice President', value: 'Vice President' },
            { key: 'secretary', text: 'Secretary', value: 'Secretary' },
            { key: 'treasurer', text: 'Treasurer', value: 'Treasurer' },
          ] }
          onChange={ handleChange }
          fluid
          selection
          upward
        />
      </Form>
      <div style={{ position: 'relative' }}>
        <Button
          style={{
            position: 'absolute',
            top: '10px',
            right: '10px',
            zIndex: 100,
          }}
          labelPosition="right"
          onClick={ handleCopy }
          icon
        ><Icon name="copy" /> Copy</Button>
        <Segment
          style={{
            marginTop: '20px',
          }}
        >
          <span ref={ copyRef }>
            <table>
              <tr>
                <td rowSpan={ 2 }>
                  <img
                    src={ theme.logo }
                    height="90px"
                    style={{ paddingRight: '20px' }}
                    alt="VicPAH Logo"
                  />
                </td>
                <td>
                  { salutation }<br/>
                  { displayName }
                </td>
              </tr>
              <tr>
                <td>
                  <em>{ position }</em><br/>
                  <em>Victorian Pups and Handlers Incorporated</em>
                </td>
              </tr>
            </table>
            <p><em>
              Victorian Pups and Handlers acknowledges Traditional Owners of
              Country throughout Australia and recognises the continuing
              connection to lands, waters and communities. We pay our respect
              to Aboriginal and Torres Strait Islander cultures; and to Elders
              past and present.
            </em></p>
          </span>
        </Segment>
      </div>
    </Container>
  );
}));
