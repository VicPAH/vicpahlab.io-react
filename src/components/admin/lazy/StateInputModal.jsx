import _ from 'lodash';
import moment from 'moment-timezone';
import React, { useCallback, useState } from 'react';
import { connect } from 'react-redux';
import { DateTimeInput } from 'semantic-ui-calendar-react';
import { Button, Form, Icon, Modal } from 'semantic-ui-react';

const formDateTimeFormat = 'YYYY-MM-DDTHH:mm:ss';
let initialDateTime = null;

export const StateInputModal = connect(
  state => ({
    refreshClient: state.refreshClient,
  }),
)(props => {
  const { id, onClose, onUpdate, refreshClient } = props;
  const [values, setValues] = useState({
    date: initialDateTime || moment().format(formDateTimeFormat),
  });
  initialDateTime = values.date;
  const handleChange = useCallback((e, { name, value }) => {
    if (!value || value.trim() === '') {
      setValues(_.omit(values, [name]));
      return
    }
    setValues({
      ...values,
      [name]: value,
    })
  }, [setValues, values]);
  const [saving, setSaving] = useState(false);
  const handleSave = useCallback(() => {
    if (!refreshClient) return;
    setSaving(true);
    refreshClient
      .post(
        `/member_state_admin/${id}`,
        {
          ...values,
          date: moment.tz(values.date, 'Australia/Melbourne').format()
        },
      )
      .then(resp => {
        onUpdate(resp.data);
        onClose();
      })
      .catch(err => {
        console.error(err);
      });
  }, [id, onClose, onUpdate, refreshClient, setSaving, values]);
  const handleSetNow = useCallback(() => {
    setValues({ ...values, date: moment().format(formDateTimeFormat) });
  }, [setValues, values]);

  if (!props.open) return null;

  console.log(props.stateSvc.state);
  return (
    <Modal { ..._.pick(props, ['open', 'onClose']) }>
      <Modal.Header>Append state log item for { id }</Modal.Header>
      <Modal.Content>
        <Form>
          <Form.Field
            control={ Form.Select }
            label="Action"
            name="action"
            disabled={ saving }
            options={
              props.stateSvc.state.nextEvents.map(a => ({
                key: a,
                value: a,
                text: a,
              }))
            }
            value={ values.action }
            onChange={ handleChange }
            required
            />
          <div style={{ display: 'flex', alignItems: 'flex-end' }}>
            <div style={{ flexGrow: 1 }}>
              <Form.Field
                control={ DateTimeInput }
                label="Date"
                name="date"
                dateTimeFormat={ formDateTimeFormat }
                disabled={ saving }
                value={ values.date }
                onChange={ handleChange }
                required
                />
            </div>
            <Button onClick={ handleSetNow }>Now</Button>
          </div>
          <Form.Field
            control={ Form.TextArea }
            label="Note"
            name="note"
            disabled={ saving }
            value={ values.note }
            onChange={ handleChange }
            />
          <Form.Field
            control={ Form.TextArea }
            label="Meta"
            name="meta"
            disabled={ saving }
            placeholder="{ ... JSON }"
            value={ values.meta }
            onChange={ handleChange }
            />
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button
          disabled={ saving || !refreshClient }
          saving={ saving }
          onClick={ handleSave }
          positive
          icon
        >
          Add <Icon name="plus" />
        </Button>
      </Modal.Actions>
    </Modal>
  );
});
