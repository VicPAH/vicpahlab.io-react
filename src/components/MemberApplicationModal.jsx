import _ from 'lodash';
import moment from 'moment-timezone';
import React, { useCallback, useEffect } from 'react';
import { connect } from 'react-redux';
import { Button, Modal, Table } from 'semantic-ui-react';

import { Handwritten } from './Handwritten';
import * as a from '../store/actions';
import { httpErrorToast } from '../utils/toast';


const doubleRowStyle = {
  fontSize: '2rem',
  whiteSpace: 'pre',
};

const DoubleRow = props => {
  const {
    first, second,
    firstWritten = false, secondWritten = true,
    width,
  } = props;
  return (
    <Table.Row>
      <Table.Cell width={ width }>
        { firstWritten
          ? <Handwritten style={ doubleRowStyle }>{ first }</Handwritten>
          : first
        }
      </Table.Cell>
      <Table.Cell>
        { secondWritten
          ? <Handwritten style={ doubleRowStyle }>{ second }</Handwritten>
          : second
        }
      </Table.Cell>
    </Table.Row>
  );
};


const fullName = member => ([
    ...(member.givenName ? [member.givenName] : []),
    ...(member.familyName ? [member.familyName] : []),
].join(' ') || null);


export const MemberApplicationModal = connect(
  state => ({
    state: state.submitApplicationState,
    errors: state.submitApplicationErrors,
  }),
  { submitApplication: a.submitApplication },
)(props => {
  const { errors, memberId, onClose, onComplete, state, submitApplication, values } = props;
  const handleNext = useCallback(() => {
    submitApplication(memberId);
  }, [submitApplication, memberId]);
  const loading = state === 'pending';
  useEffect(() => {
    if (state === 'success') {
      if (onComplete) onComplete();
    } else if (state === 'error') {
      httpErrorToast('Error submitting application', errors);
    }
  }, [state]);  // eslint-disable-line react-hooks/exhaustive-deps
  // TODO handle error
  return (
    <Modal { ..._.pick(props, ['open', 'onClose']) }>
      <Modal.Header>Submit Membership Application</Modal.Header>
      <Modal.Content>
        <p>
          I, <Handwritten>{ fullName(values) }</Handwritten> wish to express my intent to become
          a { new Date().getFullYear() } paid member of Victorian Pups &
          Handlers accepting all the benefits that a paid membership to the
          association provides. I agree that upon my acceptance from the
          committee I will be considered a member after having paid the
          membership fee and wish for this letter to be my formal application
          to be submitted to the Victorian Pups & Handlers committee.
        </p>
        <p>
          I agree to support the purpose of the association which is to provide
          community-based support to advance the diverse pup and handler
          communities of Victoria, made up of practitioners from a broad range
          of sexual identities.
        </p>
        <Table definition>
          <Table.Body>
            <DoubleRow first="Scene Name" second={ values.displayName || '-' } width={ 4 } />
            <DoubleRow first="Residential Address" second={ values.address || '-' } />
            <DoubleRow first="Email Address" second={ values.email || '-' } />
            <DoubleRow first="Mobile Number" second={ values.mobileNumber || '-' } />
          </Table.Body>
        </Table>
        <Table basic="very">
          <Table.Header>
            <Table.Row>
              <Table.Cell><strong>Signed:</strong></Table.Cell>
              <Table.Cell><strong>Date:</strong></Table.Cell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <DoubleRow first={ fullName(values) } second={ moment().format('Do MMM YYYY') } firstWritten={ true } />
          </Table.Body>
        </Table>
      </Modal.Content>
      <Modal.Actions>
        <Button content="Cancel" icon="close" labelPosition="right" onClick={ onClose } disabled={ loading } negative />
        <Button content="Agree and submit" icon="right arrow" labelPosition="right" onClick={ handleNext } loading={ loading } disabled={ loading } positive />
      </Modal.Actions>
    </Modal>
  );
});
