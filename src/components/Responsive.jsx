import React from 'react';
import styled from 'styled-components/macro';

const MinWidth = styled.div`
display: none;
@media (min-width: ${props => props.minWidth}px) {
  display: inherit;
}
`;
const MaxWidth = styled.div`
display: none;
@media (max-width: ${props => props.maxWidth}px) {
  display: inherit;
}
`;
const BothWidth = styled.div`
display: none;
@media (min-width: ${props => props.minWidth}px, max-width: ${props => props.maxWidth}px) {
  display: inherit;
}
`;

export const Responsive = props => {
  const { minWidth, maxWidth } = props;
  if (minWidth && maxWidth) {
    return <BothWidth { ...props } />
  } else if (minWidth) {
    return <MinWidth { ...props } />
  } else {
    return <MaxWidth { ...props } />
  }
};
