import _ from 'lodash';
import moment from 'moment-timezone';
import React, { Fragment, PureComponent } from 'react';
import { Header, Icon, Label, List } from 'semantic-ui-react';
import styled, { withTheme } from 'styled-components/macro';

import { DateRange } from './DateRange';
import { MaybeLink } from './MaybeLink';
import { strToColor } from '../utils/color';


/* global ICAL_HOST_REPLACE_DEF */
const ICAL_HOST_REPLACE = ICAL_HOST_REPLACE_DEF;
const eventUrl = urlStr => {
  let url
  try {
    url = new URL(urlStr);
  } catch(err) {
    return urlStr;
  }
  const toReplace = ICAL_HOST_REPLACE.find(v => v === url.host);
  if (!toReplace) {
    return urlStr;
  }
  const re = new RegExp(`^.*://${toReplace}`)
  return urlStr.replace(re, document.location.origin)
}


function* generateMonthEvents(events) {
  const tz = moment.tz.guess()
  const now = moment()
  let currentMonth = null
  let currentEvents = []
  // eslint-disable-next-line no-unused-vars
  for (const event of events) {
    const props = event._getProps()
    if (props.dtend.isBefore(now)) {
      continue
    }
    const tzDtstart = props.dtstart.tz(tz)
    const iMonth = tzDtstart.format('MMMM')
    if (iMonth !== currentMonth) {
      if (currentMonth) {
        yield [currentMonth, currentEvents]
      }
      currentMonth = iMonth
      currentEvents = [event]
    } else {
      currentEvents.push(event)
    }
  }
  yield [currentMonth, currentEvents]
}


export class Event extends PureComponent {
  render() {
    return null;
  }
}

@withTheme
class EventCategory extends PureComponent {
  static names = {
    vicpah: 'VicPAH',
    qpah: 'QPAH',
    sapah: 'SAPAH',
    sydpah: 'SydPAH',
    wapah: 'WAPAH',

    aphc: 'APHC',
    ipahw: 'IPAHW',
    qphc: 'QPHC',
  }
  static colors = {
    vicpah: { bg: function() { return this.props.theme.vicblue }, fg: 'white' },
    victoria: { bg: function() { return this.props.theme.vicblue }, fg: 'white' },
    free: { bg: 'red', fg: 'white' },
    mosh: { bg: 'purple', fg: 'white' },
    munch: { bg: 'purple', fg: 'white' },
    hangout: { bg: '#119184', fg: 'white' },

    qphc: { bg: function() { return this.props.theme.qldcolor } , fg: 'white' },
    qpah: { bg: function() { return this.props.theme.qldcolor } , fg: 'white' },
    sapah: { bg: function() { return this.props.theme.sacolor } , fg: 'white' },
    sydpah: { bg: function() { return this.props.theme.nswcolor } , fg: 'white' },
    wapah: { bg: function() { return this.props.theme.wacolor } , fg: 'white' },
  }
  static renderers = {
    major: () => null,
    vicpah: function() {
      const props = this.getColorProps('vicpah')
      return (
        <Label key="vicpah" { ...props } image>
          <MaybeLink style={{ opacity: 1 }} to={ `/events/group/vicpah` }>
            <img src={ this.props.theme.logo } style={{ height: '26px', width: '26px' }} alt="Vi
cPAH"/>
            { this.renderName('vicpah') }
          </MaybeLink>
        </Label>
      )
    },
    free: function() {
      const props = this.getColorProps('free')
      return (
        <Label key="free" { ...props } image>
          <MaybeLink style={{ opacity: 1 }} to={ `/events/group/free` }>
            <Icon.Group color="white" style={{ marginRight: '6px' }}>
              <Icon name="dollar" style={{ marginLeft: '3px' }}/>
              <Icon name="dont" size="large"/>
            </Icon.Group>
            { this.renderName('free') }
          </MaybeLink>
        </Label>
      )
    },
    mosh: function() {
      const props = this.getColorProps('munch')
      return (
        <Label key="mosh" { ...props } image>
          <MaybeLink style={{ opacity: 1 }} to={ `/events/group/mosh` }>
            <Icon name="paw" />
            { this.renderName('mosh') }
          </MaybeLink>
        </Label>
      )
    },
    munch: function() {
      const props = this.getColorProps('munch')
      return (
        <Label key="munch" { ...props } image>
          <MaybeLink style={{ opacity: 1 }} to={ `/events/group/munch` }>
            <Icon name="utensils" />
            { this.renderName('munch') }
          </MaybeLink>
        </Label>
      )
    },
  }
  renderName = name => {
    return EventCategory.names[name]
      ? EventCategory.names[name]
      : _.startCase(name)
  }
  getColorProps = name => {
    if (EventCategory.colors[name]) {
      const { bg, fg } = EventCategory.colors[name]
      return {
        style: {
          backgroundColor: _.isFunction(bg) ? bg.apply(this) : bg,
          borderColor: _.isFunction(bg) ? bg.apply(this) : bg,
          color: _.isFunction(fg) ? fg.apply(this) : fg,
        },
      }
    } else {
      return { color: strToColor(name) }
    }
  }
  render() {
    const { name } = this.props;

    if (EventCategory.renderers[name]) {
      return EventCategory.renderers[name].apply(this)
    }

    const props = this.getColorProps(name)
    return (
      <Label key={ name } {...props}>
        <MaybeLink style={{ opacity: 1 }} to={ `/events/group/${name}` }>
          { this.renderName(name) }
        </MaybeLink>
      </Label>
    )
  }
}


const EventDay = styled(Header)`
min-width: 60px;
text-align: left;
`

class EventRender extends PureComponent {
  render() {
    const evProps = this.props.event._getProps();
    return (
      <List.Item style={{ position: 'relative' }}>
        <List.Icon verticalAlign="middle">
          <EventDay>{ evProps.dtstart.format('Do')}</EventDay>
        </List.Icon>
        <List.Content>
          <List.Header>
            <MaybeLink to={ eventUrl(evProps.url) }>{ evProps.summary }</MaybeLink>
          </List.Header>
          <List.Description>
            <div>
              <DateRange start={ evProps.dtstart }
                         end={ evProps.dtend }
                         timezone={ evProps.dtstartTz || 'Australia/Melbourne' }
                         showstartday={ false }
                         showstartmonth={ false }
                         />
            </div>
            { evProps.categories.map(name => <EventCategory key={ name } name={ name } />) }
          </List.Description>
        </List.Content>
      </List.Item>
    )
  }
}

class EventsListMonth extends PureComponent {
  render() {
    const { name, events } = this.props
    return (
      <Fragment>
        <Header as="h2">{ name }</Header>
        <List size="big" divided>
          { events.map(ev => <EventRender key={ ev.getUid() } event={ ev } />) }
        </List>
      </Fragment>
    );
  }
}

export class EventsList extends PureComponent {
  render() {
    const monthEvents = Array.from(generateMonthEvents(
      React.Children
        .map(this.props.children, child => React.isValidElement(child) ? child.props.event : null )
        .filter(child => child)
    ))
    return (
      <Fragment>
        { monthEvents.length === 0
          && <Header as="h2">No future events scheduled</Header>
        }
        { monthEvents.map(([month, iEvents]) => (
          <EventsListMonth key={ month } name={ month } events={ iEvents } />
        )) }
      </Fragment>
    );
  }
}
